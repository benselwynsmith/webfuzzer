package nz.ac.vuw.httpfuzz.jee.instrumentation;

/**
 * Constants for the runtime component. Copy of nz.ac.vuw.httpfuzz.jee.rt.Constants .
 * TODO: replace by dependency, by using different project structire with mvn aggregator.
 * @author jens dietrich
 */
public interface RTConstants {


    public static final String FUZZING_FEEDBACK_TICKET_HEADER = "WEBFUZZ-FEEDBACK-TICKET";

    public static final String FUZZING_FEEDBACK_PATH_TOKEN = "/__fuzzing_feedback";

}