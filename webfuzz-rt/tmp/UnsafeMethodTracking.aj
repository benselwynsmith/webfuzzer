package nz.ac.vuw.httpfuzz.jee.instrumentation;

import nz.ac.vuw.httpfuzz.jee.rt.DataKind;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.ConstructorSignature;
import org.aspectj.lang.reflect.MethodSignature;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Aspect to record stacktraces of invocations of potenially unsafe methods.
 * @author jens dietrich
 */
public aspect UnsafeMethodTracking {

    /**
     * Tracking of selected system classes -- those might be unsafe sinks.
     * Callsites are instrumented, this avoid the tricky instrumentation of the actual system classes.
     */
    @Before(
            "(call(* java.lang.Runtime.exec(..)) || " +
                    "call(* java.lang.reflect.Method.invoke(..)) || " +
                    "call(java.lang.Class *..*.*ClassLoader.loadClass(..)) || " +
                    "call(* java.lang.System.exit(..)) || " +
                    "call(* java.io.ObjectInputStream.resolveClass(..))) && " +
                    "!within(nz.ac.vuw.httpfuzz.jee..**) "
    )
    // NOTE that we are using Java 8 features here due to an aspectj dependency
    // TODO: port this to Java 9+ stack walker API
    public void trackUnsafeSystemSinks(JoinPoint joinPoint) {
        // System.out.println("UNSAFE SINK ENCOUNTERED: " + joinPoint);
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        List<String> encodedStackTrace = Stream.of(stackTrace).map(ste -> ste.toString()).collect(Collectors.toList());
        removeInstrumentationFramesFromStacktrace(encodedStackTrace);

        // the actual unsafe method is not yet on the stack, so this needs to be added
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        String className = method.getDeclaringClass().getName();
        String methodName = method.getName();
        String descr = extractDescriptor(method);
        encodedStackTrace.add(0, className + '.' + methodName);

        nz.ac.vuw.httpfuzz.jee.rt.InvocationTracker.DEFAULT.track(DataKind.unsafeSinkInvocationStackTraces,encodedStackTrace);

        // this seems redundant, but is necessary as multiple instrumentations of the same method seem to interfer !!
        nz.ac.vuw.httpfuzz.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods,className + "::"+ methodName + "::"+ descr);
    }

    // filter out the instrumentation related top part of the stack
    private void removeInstrumentationFramesFromStacktrace(List<String> encodedStackTrace) {
        int instrumentationIndex = -1;
        for (int i = 0; i < encodedStackTrace.size(); i++) {
            if (encodedStackTrace.get(i).startsWith(UnsafeMethodTracking.class.getName())) {
                instrumentationIndex = i;
                break;
            }
        }
        if (instrumentationIndex > -1) {
            for (int i = 0; i <= instrumentationIndex; i++) {
                encodedStackTrace.remove(0);
            }
        }
    }

    @Before(
            "call(java.lang.Thread.new(..)) "
    )
    public void trackUnsafeSystemSinks2(JoinPoint joinPoint) {

        // System.out.println("UNSAFE SINK ENCOUNTERED: " + joinPoint);
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        List<String> encodedStackTrace = Stream.of(stackTrace).map(ste -> ste.toString()).collect(Collectors.toList());
        removeInstrumentationFramesFromStacktrace(encodedStackTrace);

        // the actual unsafe method is not yet on the stack, so this needs to be added
        Signature signature = joinPoint.getSignature();
        ConstructorSignature methodSignature = (ConstructorSignature) signature;
        Constructor constructor = methodSignature.getConstructor();
        String className = constructor.getDeclaringClass().getName();
        String descr = extractDescriptor(constructor);
        encodedStackTrace.add(0, className + '.' + "<init>");

        nz.ac.vuw.httpfuzz.jee.rt.InvocationTracker.DEFAULT.track(DataKind.unsafeSinkInvocationStackTraces,encodedStackTrace);

        // this seems redundant, but is necessary as multiple instrumentations of the same method seem to interfer !!
        nz.ac.vuw.httpfuzz.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods,className + "::"+ "<init>" + "::"+ descr);

    }

    private String extractDescriptor(Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        boolean f = true;
        for (Class paramType : method.getParameterTypes()) {
            if (f) f = false;
            else sb.append(',');
            sb.append(paramType.getName());
        }
        sb.append(')');
        sb.append(method.getReturnType().getName());
        return sb.toString();
    }

    private String extractDescriptor(Constructor constructor) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        boolean f = true;
        for (Class paramType : constructor.getParameterTypes()) {
            if (f) f = false;
            else sb.append(',');
            sb.append(paramType.getName());
        }
        sb.append(')');
        return sb.toString();
    }

}