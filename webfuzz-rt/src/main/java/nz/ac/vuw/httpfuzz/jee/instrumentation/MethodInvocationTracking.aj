package nz.ac.vuw.httpfuzz.jee.instrumentation;

import nz.ac.vuw.httpfuzz.jee.rt.DataKind;
import org.aspectj.lang.*;
import org.aspectj.lang.reflect.*;
import org.aspectj.lang.annotation.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Aspect to record invocations of methods.
 * @author jens dietrich
 */
public aspect MethodInvocationTracking {


    // @Before("execution(* *.*(..)) && !within(nz.ac.vuw.httpfuzz.jee.**) && !within(nz.ac.vuw.httpfuzz.jee.rt.**) && !within(nz.ac.vuw.httpfuzz.jee.instrumentation.**) && !within(nz.ac.vuw.httpfuzz.jee.**) && !within(nz.ac.vuw.httpfuzz.jee.shadowed.org.json.**)" + EXCLUSIONS)
    @Before("execution(* *.*(..)) && !within(nz.ac.vuw.httpfuzz.jee.rt.*) && !within(nz.ac.vuw.httpfuzz.jee.instrumentation.*)")
    public void trackMethodInvocation(JoinPoint joinPoint) {
        org.aspectj.lang.Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        String className = method.getDeclaringClass().getName();
        String methodName = method.getName();
        String descr = extractDescriptor(method);
        nz.ac.vuw.httpfuzz.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods,className + "::"+ methodName + "::"+ descr);
        //nz.ac.vuw.httpfuzz.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods,className + "::"+ methodName + "::"+ descr);
    }

//     @Before("execution(*.new(..)) && !within(nz.ac.vuw.httpfuzz.jee.**) && !within(nz.ac.vuw.httpfuzz.jee.rt.**) && !within(nz.ac.vuw.httpfuzz.jee.instrumentation.**) && !within(nz.ac.vuw.httpfuzz.jee.**) && !within(nz.ac.vuw.httpfuzz.jee.shadowed.org.json.**)")
    @Before("execution(*.new(..)) && !within(nz.ac.vuw.httpfuzz.jee.rt.*) && !within(nz.ac.vuw.httpfuzz.jee.instrumentation.*)")
    public void trackConstructorInvocation(JoinPoint joinPoint) {
        // defensive, this causes issues with inner classes in jenkins (org.kohsuke.stapler.Function)
        try {
            if (joinPoint != null) {
                org.aspectj.lang.Signature signature = joinPoint.getSignature();
                ConstructorSignature constructorSignature = (ConstructorSignature) signature;
                Constructor constructor = constructorSignature.getConstructor();
                String className = constructor.getDeclaringClass().getName();
                String methodName = "<init>";
                String descr = extractDescriptor(constructor);
                nz.ac.vuw.httpfuzz.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods, className + "::" + methodName + "::" + descr);
            }
        }
        catch (Throwable t){}
        // nz.ac.vuw.httpfuzz.jee.rt.InvocationTracker.DEFAULT.track(DataKind.invokedMethods,className + "::"+ methodName + "::"+ descr);
    }


    private String extractDescriptor(Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        boolean f = true;
        for (Class paramType : method.getParameterTypes()) {
            if (f) f = false;
            else sb.append(',');
            sb.append(paramType.getName());
        }
        sb.append(')');
        sb.append(method.getReturnType().getName());
        return sb.toString();
    }

    private String extractDescriptor(Constructor constructor) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        boolean f = true;
        for (Class paramType : constructor.getParameterTypes()) {
            if (f) f = false;
            else sb.append(',');
            sb.append(paramType.getName());
        }
        sb.append(')');
        return sb.toString();
    }

}