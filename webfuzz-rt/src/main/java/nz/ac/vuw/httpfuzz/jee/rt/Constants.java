package nz.ac.vuw.httpfuzz.jee.rt;

/**
 * Constants used for the feedback mechanism.
 * @author jens dietrich
 */
public class Constants {

    public static final String FUZZING_FEEDBACK_TICKET_HEADER = "WEBFUZZ-FEEDBACK-TICKET";

    public static final String FUZZING_FEEDBACK_PATH_TOKEN = "/__fuzzing_feedback";

}
