package nz.ac.vuw.httpfuzz.jee.rt;

/**
 * Utilities.
 * @author jens dietrich
 */
public class Util {

    static boolean isArray(Object obj) {
        return obj!=null && obj.getClass().isArray();
    }

}
