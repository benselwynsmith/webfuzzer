package nz.ac.vuw.httpfuzz.jee.rt;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter to manage the lifecycle of the invocation tracker.
 * @author jens dietrich
 */
public class InvocationTrackerManagerFilter implements Filter {

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String path = ((HttpServletRequest)request).getServletPath();
        boolean isApplicationRequest = !path.contains(Constants.FUZZING_FEEDBACK_PATH_TOKEN);
        if (isApplicationRequest) {
            String ID = InvocationTracker.DEFAULT.startInvocationTracking();
            ((HttpServletResponse) response).addHeader(Constants.FUZZING_FEEDBACK_TICKET_HEADER, ID);
        }

        try {
            chain.doFilter(request, response);
        }
        catch (Exception x) {
            // catch exception to continue in case  an exception leads to a 500
            if (isApplicationRequest) {
                InvocationTracker.DEFAULT.finishInvocationTracking();
            }
            throw new ServletException("wrapped application exception",x);
        }

        if (isApplicationRequest) {
            InvocationTracker.DEFAULT.finishInvocationTracking();
        }

    }

    public void destroy() {
    }

    public void init(FilterConfig filterConfig) {
    }

}

