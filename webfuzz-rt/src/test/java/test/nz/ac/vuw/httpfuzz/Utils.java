package test.nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.jee.rt.DataKind;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

/**
 * Misc utils uses across tests.
 * @author jens dietrich
 */
public class Utils {

    static List<MethodSpec> extractInvocations(JSONObject json) {
        JSONArray invocationData = json.getJSONArray(DataKind.invokedMethods.name());
        List<MethodSpec> methods = new ArrayList<>(invocationData.length());
        for (int i=0;i<invocationData.length();i++) {
            String def = invocationData.getString(i);
            String[] parts = def.split("::");
            MethodSpec methodSpec = new MethodSpec(parts[0],parts[1],parts[2]);
            methods.add(methodSpec);
        }
        return methods;
    }

    static List<String> extractRequestParameterNames(JSONObject json) {
        JSONArray reqParamData = json.getJSONArray(DataKind.requestParameterNames.name());
        List<String> names = new ArrayList<>();
        for (int i=0;i<reqParamData.length();i++) {
            names.add(reqParamData.getString(i));
        }
        return names;
    }

    static List<String> extractStackTraces(JSONObject json) {
        JSONArray stackTraceData = json.getJSONArray(DataKind.unsafeSinkInvocationStackTraces.name());
        List<String> stackTraces = new ArrayList<>();
        for (int i=0;i<stackTraceData.length();i++) {
            stackTraces.add(stackTraceData.getString(i));
        }
        return stackTraces;
    }

     static List<Map<String, List<String>>> extractUnsafesinkDataflows(JSONObject json) {

       JSONArray unsafeSinkDataflows = json.getJSONArray("unsafeSinkDataflows");

       List<Map<String, List<String>>> res = new ArrayList<>();

       for(int i=0; i < unsafeSinkDataflows.length(); i++) {
           JSONArray flows = unsafeSinkDataflows.getJSONObject(i).getJSONArray("dataflows");
           JSONArray traces = unsafeSinkDataflows.getJSONObject(i).getJSONArray("stacktrace");
           Map<String, List<String>> entry = new HashMap<>();
           entry.put("dataflows", Arrays.asList(flows.getString(0).split("\n")));
           entry.put("stacktrace", Arrays.asList(traces.getString(0).split("\n")));
           res.add(entry);

       }
       return res;

    }

}


