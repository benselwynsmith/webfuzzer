package test.nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.jee.rt.*;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockFilterConfig;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.*;

public class ServletWithInvocationsOfSystemSinksTest {

    @Test
    public void testInvocationTracking () throws Exception {

        // part 1: record invocations
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterConfig config = new MockFilterConfig();
        FilterChain chain = new MockFilterChain() {
            @Override
            public void doFilter(ServletRequest req, ServletResponse res) throws ServletException, IOException {
            new ServletWithInvocationsOfSystemSinks().doGet((HttpServletRequest)req,(HttpServletResponse)res);
            }
        };

        InvocationTrackerManagerFilter invocationTrackerManagerFilter = new InvocationTrackerManagerFilter();
        invocationTrackerManagerFilter.init(config);
        invocationTrackerManagerFilter.doFilter(request,response,chain);
        invocationTrackerManagerFilter.destroy();

        String ticket = response.getHeader(Constants.FUZZING_FEEDBACK_TICKET_HEADER);

        assertNotNull(ticket);

        // part 2: pick up recorded invocations
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        TrackedInvocationsPickupServlet pickup = new TrackedInvocationsPickupServlet();
        request.setPathInfo(ticket);
        pickup.doGet(request,response);

        assertEquals("application/json",response.getContentType());

        JSONObject json = new JSONObject(response.getContentAsString());

        List<MethodSpec> specs = Utils.extractInvocations(json);

        System.out.println("= INVOKED METHODS =");
        for (MethodSpec methodSpec:specs) {
            System.out.println(methodSpec);
        }
        System.out.println("=========");

        assertTrue(specs.stream()
            .filter(m -> m.getClassName().equals("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks"))
            .filter(m -> m.getMethodName().equals("foo1"))
            .anyMatch(m -> m.getDescriptor().equals("()void")));

        assertTrue(specs.stream()
            .filter(m -> m.getClassName().equals("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks"))
            .filter(m -> m.getMethodName().equals("foo2"))
            .anyMatch(m -> m.getDescriptor().equals("()void")));

        assertTrue(specs.stream()
            .filter(m -> m.getClassName().equals("java.lang.Runtime"))
            .anyMatch(m -> m.getMethodName().equals("exec")));

        assertTrue(specs.stream()
            .filter(m -> m.getClassName().equals("java.lang.reflect.Method"))
            .anyMatch(m -> m.getMethodName().equals("invoke")));

        assertTrue(specs.stream()
            .filter(m -> m.getClassName().equals("java.lang.Thread"))
            .anyMatch(m -> m.getMethodName().equals("<init>")));

        // part 3: try again, should have been deleted now once data has been picked up        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        pickup = new TrackedInvocationsPickupServlet();
        request.setPathInfo(ticket);
        pickup.doGet(request,response);
        assertEquals(404,response.getStatus());

    }


    @Test
    public void testStackTraceTracking () throws Exception {

        // part 1: record invocations
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        FilterConfig config = new MockFilterConfig();
        FilterChain chain = new MockFilterChain() {
            @Override
            public void doFilter(ServletRequest req, ServletResponse res) throws ServletException, IOException {
                new ServletWithInvocationsOfSystemSinks().doGet((HttpServletRequest)req,(HttpServletResponse)res);
            }
        };

        InvocationTrackerManagerFilter invocationTrackerManagerFilter = new InvocationTrackerManagerFilter();
        invocationTrackerManagerFilter.init(config);
        invocationTrackerManagerFilter.doFilter(request,response,chain);
        invocationTrackerManagerFilter.destroy();

        String ticket = response.getHeader(Constants.FUZZING_FEEDBACK_TICKET_HEADER);

        assertNotNull(ticket);

        // part 2: pick up recorded invocations
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        TrackedInvocationsPickupServlet pickup = new TrackedInvocationsPickupServlet();
        request.setPathInfo(ticket);
        pickup.doGet(request,response);

        assertEquals("application/json",response.getContentType());

        JSONObject json = new JSONObject(response.getContentAsString());

        // part 3: test stacktraces
        List<String> stacktraces = Utils.extractStackTraces(json);

        System.out.println("= STACK TRACES OF UNSAFE METHODS =");
        for (String stackTrace:stacktraces) {
            System.out.println();
            String[] frames = stackTrace.split("\t");
            for (String frame:frames) {
                System.out.println(frame);
            }
        }
        System.out.println("=========");

        assertTrue(stacktraces.stream()
                .filter(st -> st.startsWith("java.lang.Thread.<init>"))
                .filter(st -> st.contains("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks.foo2"))
                .filter(st -> st.contains("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks.foo1"))
                .filter(st -> st.contains("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks.doGet"))
                .anyMatch(st -> true));

        assertTrue(stacktraces.stream()
                .filter(st -> st.startsWith("java.lang.Runtime.exec"))
                .filter(st -> st.contains("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks.foo2"))
                .filter(st -> st.contains("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks.foo1"))
                .filter(st -> st.contains("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks.doGet"))
                .anyMatch(st -> true));

        assertTrue(stacktraces.stream()
                .filter(st -> st.startsWith("java.lang.reflect.Method.invoke"))
                .filter(st -> st.contains("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks.foo2"))
                .filter(st -> st.contains("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks.foo1"))
                .filter(st -> st.contains("test.nz.ac.vuw.httpfuzz.ServletWithInvocationsOfSystemSinks.doGet"))
                .anyMatch(st -> true));

        // part 3: try again, should have been deleted now once data has been picked up
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        pickup = new TrackedInvocationsPickupServlet();
        request.setPathInfo(ticket);
        pickup.doGet(request,response);
        assertEquals(404,response.getStatus());

    }

}
