package nz.ac.vuw.httpfuzz.jee.war;

import nz.ac.vuw.httpfuzz.EntryPoint;
import nz.ac.vuw.httpfuzz.Profile;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * A more advanced implementation of a war scope model that performs a static pre analysis.
 * In particular, it uses the following heuristics:
 * - start analysis with entry points
 * - add (literals from) classes in same jar if entry points are in a jar inside the war  TODO
 * - add (literals from) classes in similar packages (with a common super string)  TODO
 * @author jens dietrich
 */
public class AdvancedWarScopeModel implements WarScopeModel {

    private static final String NULL = "null"; // placeholder
    private File war = null;
    private static final Logger LOGGER = Logger.getLogger("adv-war-scope-model");
    private Set<Pair<String,String>> pathsToApplicationClasses = null;

    public AdvancedWarScopeModel(File war, boolean addClassesInSameContainerAsEntryPointsToApplicationScope, Profile profile) throws Exception {
        this.war = war;
        init(addClassesInSameContainerAsEntryPointsToApplicationScope,profile);
    }

    private void init(boolean addClassesInSameContainerAsEntryPointsToApplicationScope,Profile profile) throws Exception {
        LOGGER.info("Extracting entry points for scope model");
        // note -- this is done twice now (the overall JEEModel is also doing this)
        // the API is cleaner doing it again here, and it is fast and should be deterministic
        Set<EntryPoint> entryPoints = EntryPointExtractor.extractEntryPoints(war,profile);
        Set<String> entryClassLocations = entryPoints.stream()
            .map(ep -> ep.getClassName())
            .map(n -> n.replace('.','/'))
            .filter(n -> !RT.isFuzzerRuntimeClass(n))
            .collect(Collectors.toSet());

        Set<Pair<String,String>> pathsToEntryClasses = new HashSet<>();
        visitClassEntries(war,
            e -> {
                assert e.getName().endsWith(".class");
                assert e.getName().startsWith("WEB-INF/classes");
                String className = e.getName().substring(0,e.getName().length()-6);
                className = className.substring(15);
                if (entryClassLocations.contains(className)) {
                    pathsToEntryClasses.add(Pair.of(NULL,e.getName()));
                }
            },
            (jarName,e) -> {
                assert e.getName().endsWith(".class");
                String className = e.getName().substring(0,e.getName().length()-6);
                if (entryClassLocations.contains(className)) {
                    pathsToEntryClasses.add(Pair.of(jarName,e.getName()));
                }
            });
        LOGGER.info("Classes with entry points extracted: " + pathsToEntryClasses.size());

        pathsToApplicationClasses = new HashSet<>();
        pathsToApplicationClasses.addAll(pathsToEntryClasses);

        if (addClassesInSameContainerAsEntryPointsToApplicationScope) {
            LOGGER.info("Extracting classes in same container as entry points");
            Set<String> containersWithEntryPoints = pathsToEntryClasses.stream().map(p -> p.getLeft()).collect(Collectors.toSet());
            Set<Pair<String, String>> pathsToClassesInSameLocationAsEntryPoints = new HashSet<>();
            visitClassEntries(war,
                    e -> {
                        assert e.getName().endsWith(".class");
                        assert e.getName().startsWith("WEB-INF/classes");
                        if (containersWithEntryPoints.contains(NULL)) {
                            pathsToClassesInSameLocationAsEntryPoints.add(Pair.of(NULL, e.getName()));
                        }
                    },
                    (jarName, e) -> {
                        assert e.getName().endsWith(".class");
                        if (containersWithEntryPoints.contains(jarName)) {
                            pathsToClassesInSameLocationAsEntryPoints.add(Pair.of(jarName, e.getName()));
                        }
                    });
            LOGGER.info("Classes in same container as entry points extracted: " + pathsToClassesInSameLocationAsEntryPoints.size());
            pathsToApplicationClasses.addAll(pathsToClassesInSameLocationAsEntryPoints);
        }

        LOGGER.info("Application classes extracted: " + pathsToApplicationClasses.size());

    }

    @Override
    public boolean isApplicationClass(String outerName, String innerName) {
        if (isFuzzerRuntimeClass(outerName,innerName)) return false;
        return pathsToApplicationClasses.contains(Pair.of(outerName,innerName));
    }

    @Override
    public boolean isLibClass(String outerName, String innerName) {
        if (isFuzzerRuntimeClass(outerName,innerName)) return false;
        return !pathsToApplicationClasses.contains(Pair.of(outerName,innerName));
    }

    private void visitClassEntries (File war, Consumer<ZipEntry> classVisitor, BiConsumer<String,ZipEntry> classInJarVisitor) throws Exception {
        ZipFile zip = new ZipFile(war);
        Enumeration<? extends ZipEntry> en = zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.startsWith("WEB-INF/classes/") && name.endsWith(".class")) {
                classVisitor.accept(e);
            }
            if (name.startsWith("WEB-INF/lib/") && name.endsWith(".jar")) {
                InputStream in = zip.getInputStream(e);
                ZipInputStream input = new ZipInputStream(in);
                ZipEntry e2 = null;
                while ((e2 = input.getNextEntry()) != null ) {
                    if (e2.getName().endsWith(".class")) {
                        classInJarVisitor.accept(name,e2);
                    }
                }
                input.close();
            }
        }
    }

}
