package nz.ac.vuw.httpfuzz.jee.war;

import nz.ac.vuw.httpfuzz.commons.LogSystem;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import static nz.ac.vuw.httpfuzz.jee.war.RT.isFuzzerRuntimeClass;

/**
 * Utility to extract resource bundle keys from wars.
 * The idea is that those appear as string literals in byte code when resource bundles are queried,
 * but they are not useful for fuzzing.
 * @author jens dietrich
 */
public class ResourceBundleKeyExtractor<T> {

    public static final Logger LOGGER = LogSystem.getLogger(ResourceBundleKeyExtractor.class);

    protected boolean isResourceBundle(String name) {
        return name.endsWith(".properties");
    }

    public Set<String> extract (File war,boolean  extractFromAppClasses,boolean extractFromLibClasses) throws Exception {
        ZipFile zip = new ZipFile(war);
        Enumeration<? extends ZipEntry> en = zip.entries();
        Set<String> keys = new HashSet<>();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (extractFromAppClasses && name.startsWith("WEB-INF/classes/") && isResourceBundle(name)) {
                InputStream in = zip.getInputStream(e);
                addKeys(in,keys);
                in.close();
            }
            else if (extractFromLibClasses && name.startsWith("WEB-INF/lib/") && name.endsWith(".jar")) {
                InputStream in = zip.getInputStream(e);
                try (ZipInputStream input = new ZipInputStream(in)) {
                    ZipEntry e2 = null;
                    while ((e2 = input.getNextEntry()) != null) {
                        if (isResourceBundle(e2.getName())) {
                            // InputStream in2 = zip.getInputStream(e2);
                            addKeys(input, keys);
                        }
                    }
                }
            }
        }
        return keys;
    }

    private void addKeys(InputStream in, Set<String> keys) {
        try {
            BufferedReader reader = new BufferedReader((new InputStreamReader(in)));
            String line = null;
            while ((line = reader.readLine())!=null) {
                // ignore comments and empty lines
                if (!(line.startsWith("#") || line.startsWith("!") || line.trim().isEmpty())) {
                    // key value can be separated by = : or \t
                    String[] tokens = line.split("=|:|\\t");
                    if (tokens.length==2) {
                        keys.add(tokens[0]);
                    }
                }
            }
        }
        catch (Exception x) {
            LOGGER.error("Error extracting resource bundle names",x);
        }
    }


}
