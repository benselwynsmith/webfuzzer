package nz.ac.vuw.httpfuzz.jee;

import nz.ac.vuw.httpfuzz.commons.LogSystem;
import org.apache.log4j.Logger;

/**
 * Central definition of some frequently used loggers.
 */
public interface Loggers {

    public static Logger FUZZED_SERVER = LogSystem.getLogger("fuzzed-server");
    public static Logger FUZZER = LogSystem.getLogger("fuzzer");

    public static Logger COVERAGE_METHODS = LogSystem.getLogger("coverage-methods");
    public static Logger COVERAGE_REQUEST_PARAMETERS = LogSystem.getLogger("coverage-request-parameters");
    public static Logger COVERAGE_FORMS = LogSystem.getLogger("coverage-forms");

    public static Logger RESULTS_SINK_METHODS = LogSystem.getLogger("results-sinkmethods");
    public static Logger RESULTS_DATAFLOW_TO_THINK = LogSystem.getLogger("results-dataflowtosink");
    public static Logger RESULTS_500 = LogSystem.getLogger("results-500");
    public static Logger RESULTS_XSS = LogSystem.getLogger("results-xss");
}