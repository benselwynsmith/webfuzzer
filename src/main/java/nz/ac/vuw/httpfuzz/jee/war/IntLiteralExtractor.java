package nz.ac.vuw.httpfuzz.jee.war;

import nz.ac.vuw.httpfuzz.commons.LogSystem;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static nz.ac.vuw.httpfuzz.jee.war.RT.isFuzzerRuntimeClass;

/**
 * Utility to extract int literals from a war.
 * @author jens dietrich
 */
public class IntLiteralExtractor extends AbstractWarClassAnalyser<Integer> {

    public IntLiteralExtractor(WarScopeModel scopeModel) {
        super(scopeModel);
    }

    @Override
    public Set<Integer> extractFromClassFile(InputStream in) throws Exception {
        LiteralExtractor literalExtractor = new LiteralExtractor();
        return literalExtractor.extractIntLiterals(in);
    }

}
