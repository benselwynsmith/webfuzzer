package nz.ac.vuw.httpfuzz.jee.war;

import static nz.ac.vuw.httpfuzz.jee.war.RT.isFuzzerRuntimeClass;

/**
 * A simple implementation of a war scope model, based on the idea that application classes are in WEB-INF/classes/
 * whereas lib classes are in jars in WEB-INF/lib/
 * @author jens dietrich
 */
public class SimpleWarScopeModel implements WarScopeModel {

    @Override
    public boolean isApplicationClass(String outerName, String innerName) {
        if (isFuzzerRuntimeClass(outerName,innerName)) return false;
        return innerName.startsWith("WEB-INF/classes/") && innerName.endsWith(".class");
    }

    @Override
    public boolean isLibClass(String outerName, String innerName) {
        if (isFuzzerRuntimeClass(outerName,innerName)) return false;
        return outerName!=null && outerName.startsWith("WEB-INF/lib/") && outerName.endsWith(".jar") && innerName.endsWith(".class");
    }

}
