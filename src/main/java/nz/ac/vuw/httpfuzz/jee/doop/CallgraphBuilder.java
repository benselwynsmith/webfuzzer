package nz.ac.vuw.httpfuzz.jee.doop;

import nz.ac.vuw.httpfuzz.stan.callgraph.Callgraph;
import nz.ac.vuw.httpfuzz.stan.callgraph.Invocation;
import nz.ac.vuw.httpfuzz.stan.callgraph.Method;
import java.io.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Utility to build callgraph from the CallGraphEdge.csv files produced by a doop analysis.
 * @author jens dietrich
 */
public class CallgraphBuilder {

    public static class Config {
        // Filters can be used to restrict what is included in the callgraph, and edge is added if tests for all filters pass.
        // the line filter is applied to the raw definitions in doop format, this is the most effective way of excluding information
        // in terms of parsing speed.
        public Predicate<Method> sourceFilter = m -> true;
        public Predicate<Method> targetFilter = m -> true;
        public Predicate<String> lineFilter = s -> true;
    }

    public Callgraph build (File file,Config config) throws IOException {
        return build(new FileReader(file),config);
    }

    public Callgraph build (File file) throws IOException {
        return build(file,new Config());
    }

    public Callgraph build (InputStream in,Config config) throws IOException {
        return build(new InputStreamReader(in),config);
    }

    public Callgraph build (InputStream in) throws IOException {
        return build(in,new Config());
    }

    public Callgraph build (Reader in) throws IOException {
        return build(in,new Config());
    }

    public Callgraph build (Reader in,Config config) throws IOException {
        try (BufferedReader reader = new BufferedReader(in)) {
            return build(reader.lines(),config);
        }
    }

    public Callgraph build (Stream<String> lines, Config config) {
        Callgraph callgraph = new Callgraph();
        lines.forEach( line -> {
            if (config.lineFilter.test(line)) {
                String[] tokens = line.split("\t");
                assert tokens.length == 4;
                String src = tokens[1];
                int sep = src.indexOf('/');
                String source = src.substring(0, sep);
                Method sourceMethod = callgraph.getOrAdd(source, s -> parse(s));

                if (config.sourceFilter.test(sourceMethod)) {
                    String callsite = src.substring(sep);
                    String target = tokens[3];
                    Method targetMethod = callgraph.getOrAdd(target, t -> parse(t));

                    if (config.targetFilter.test(targetMethod)) {
                        Invocation inv = new Invocation(sourceMethod, targetMethod, callsite);
                        callgraph.add(inv);
                    }
                }
            }
        });

        return callgraph;
    }


    static Method parse(String t) {
        // <JEEDriver: void main(java.lang.String[])>
        assert t.charAt(0)=='<';
        assert t.charAt(t.length()-1)=='>';
        t = t.substring(1,t.length()-1);
        String[] tokens = t.split(" ");
        assert tokens.length==3;
        String typeName = tokens[0];
        assert typeName.charAt(typeName.length()-1)==':';
        typeName = typeName.substring(0,typeName.length()-1);
        String returnTypeName = tokens[1];
        String rest = tokens[2];
        int pos = rest.indexOf('(');
        assert pos>0;
        assert rest.charAt(rest.length()-1)==')';
        String methodName = rest.substring(0,pos);
        String params = rest.substring(pos);
        String descriptor = params+returnTypeName;

        return new Method(t,typeName,methodName,descriptor);

    }

}
