package nz.ac.vuw.httpfuzz.jee;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.*;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import java.io.*;
import java.util.*;

/**
 * Executable to fuzz a war file.
 * @author jens dietrich
 */
public class JEEFuzzer extends AbstractFuzzer {

    // private AtomicInteger duplicateCounter = new AtomicInteger();
    private static int logProgressInterval = 100;

    private File war = null;
    private String contextName = "fuzzed";
    private Deployment deployment = null;

    public static void main(String[] args) throws Exception {

        Options options = new Options()
            .addOption("war", true, "The (instrumented) war file to be fuzzed (required)")
            // .addOption("doopOutput", true, "The the folder containing the doop preanalysis (required)")
            .addOption("logInterval", true, "The interval to report progress using logging (optional, default is " + logProgressInterval + ")")
            .addOption("trials", true, "The max number of generated requests (optional, default is " + DEFAULT_TRIALS + ")")
            .addOption("authAddress", true, "URL path for login request. E.g. '/login'")
            .addOption("authParams", true, "Comma separated list of key value pairs for login request. E.g. 'key1=value1,key2=value2...'")
            .addOption("contextName", true, "Context name, this is the name of the web application that will be deployed during the fuzzing campaign'")
            .addOption("deployment", true, "Deployment -- adapter for a particular server used for the fuzzing campaign, name of an instantiable class implementing nz.ac.vuw.httpfuzz.jee.Deployment")
            .addOption("randomseed", true, "Control randomness for the fuzzing run")
            .addOption("httpKeepAlive", true, "Set http client to use persistent connections");

        CommandLine cmd = new DefaultParser().parse(options, args);
        if (!cmd.hasOption("war")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java <options> " + JEEFuzzer.class.getName(), options);
            System.exit(0);
        }

        JEEFuzzer fuzzer = new JEEFuzzer();
        fuzzer.fuzz(cmd);
    }

    @Override
    protected void init(CommandLine cmd) throws Exception {
        super.init(cmd);

        this.war = new File(cmd.getOptionValue("war"));
        Preconditions.checkState(this.war.exists(),"War file " + this.war.getAbsolutePath() + " does not exist");

        if (cmd.hasOption("deployment")) {
            String deploymentClassName = cmd.getOptionValue("deployment");
            try {
                Class clazz = Class.forName(deploymentClassName);
                this.deployment = (Deployment)clazz.newInstance();
                Loggers.FUZZER.info("Deploying fuzzing campaign using " + clazz.getName());
            }
            catch (Exception x) {
                Loggers.FUZZER.error("Cannot deploy",x);
                System.exit(1);
            }
        }
        else {
            Loggers.FUZZER.warn("No deployment set, using default: " + DeployWithJetty9.class.getName());
            this.deployment = new DeployWithJetty9();
        }

        if (cmd.hasOption("contextName")) {
            contextName = cmd.getOptionValue("contextName");
        }

        Profile profile = new Profile.WebApplicationProfile(deployment.getHost(),deployment.getPort(),contextName);
        Context context = new Context(
                profile,
                new JEEStaticModel(war,s->true,profile),  // callgraph not used in example
                new DefaultDynamicModel()
        );
        context.install(); // will be referenced by generators as singleton

        this.addListener(new LoggingFuzzerListener(logProgressInterval));
        this.addListener(new StatsRecorderFuzzerListener(60_000));
    }


    @Override
    protected void deploy(CommandLine cmd) throws Exception {

        boolean deploymentStatus = deployment.deploy(
            war,
            new File("tools/aspectjweaver-1.9.4.jar"),
            contextName);

        if (deploymentStatus) {
            Loggers.FUZZER.info("Web application deployed successfully for fuzzing campaign: " + deployment.getURL());
            final Runnable shutdown = new Runnable() {
                @Override public void run() {
                    ACTIVE.set(false);
                    try {
                        deployment.undeploy();
                    } catch (Exception e) {
                        Loggers.FUZZER.error("Error undeploying web application",e);
                    }
                }
            };

            Runtime.getRuntime().addShutdownHook(new Thread(shutdown));
        }
        else {
            Loggers.FUZZER.error("Error deploying web application");
        }

        FuzzerListener listener = deployment.getMaintenanceListener();
        if (listener!=null) {
            this.addListener(listener);
        }
        startTime = System.currentTimeMillis();
    }

    @Override
    protected void undeploy(CommandLine cmd) throws Exception {
        try {
            deployment.undeploy();
            Loggers.FUZZER.info("fuzzing campaign stopped");
        }
        catch (Throwable x) {
            Loggers.FUZZER.warn("Error stopping fuzzing campaign, server may still be running on " + deployment.getHost() + ":" + deployment.getPort() + "  and needs to be shutdown manually");
        }
    }

    @Override
    protected void tearDown() throws Exception {
        Context context = Context.getDefault();
        Collection<RequestSpec> topRequests = context.getDynamicModel().getTopRequestSpecs();
        Map<RequestSpec, Object> targets = context.getDynamicModel().getTargets();

        // some reporting
        long runtime = System.currentTimeMillis() - startTime;
        assert runtime>0;
        Loggers.FUZZER.info("Tried " + trials + " requests in " + runtime + " ms");

        // TODO deprecate this, log details are better
        Loggers.FUZZER.info("" + topRequests.size() + " top requests registered");
        Loggers.FUZZER.info("" + targets.size() + " targets registered");

        Loggers.FUZZER.info("TARGET DETAILS:");
        for (RequestSpec req:targets.keySet()) {
            Loggers.FUZZER.info("\t" + req.getUriSpec().toURI() + " -> " + targets.get(req));
        }

        Loggers.FUZZER.info("TOP REQUEST DETAILS:");
        for (RequestSpec req:topRequests) {
            Loggers.FUZZER.info("\t" + req.getUriSpec().toURI());
        }

        Loggers.FUZZER.info("check logs for details ! ");
    }

}
