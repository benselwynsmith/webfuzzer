package nz.ac.vuw.httpfuzz.jee.doop;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.commons.LogSystem;
import org.apache.log4j.Logger;
import java.io.*;
import java.text.NumberFormat;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Utility to produce a reduced doop VarPointTo file by applying the configuration line filter.
 * @author jens dietrich
 */
public class DoopVarPointsFileReducer {

    private static Logger LOGGER = LogSystem.getLogger(DoopVarPointsFileReducer.class);
    public static void main(String[] args) throws Exception {
        Preconditions.checkArgument(args.length==2);
        File input = new File(args[0]);
        Preconditions.checkArgument(input.exists());
        File output = new File(args[1]);
        try (PrintWriter out = new PrintWriter(new FileWriter(output))) {
            try (BufferedReader breader = new BufferedReader(new FileReader(input))) {
                Stream<String> lines = breader.lines();
                Predicate<String> lineFilter = new PointstoBuilder.Config().lineFilter;
                AtomicInteger counter = new AtomicInteger();
                lines.forEach(line -> {
                    if (counter.incrementAndGet() % 1_000_000 == 0) {
                        LOGGER.info("\t" + NumberFormat.getInstance().format(counter.intValue()) + " lines processed");
                    }
                    if (lineFilter.test(line)) {
                        out.println(line);
                    }
                });
            }
        }

        catch (Exception x) {
            LOGGER.error(x);
            throw x;
        }

        LOGGER.info("Reduced file written to " + output.getAbsolutePath());
    }
}
