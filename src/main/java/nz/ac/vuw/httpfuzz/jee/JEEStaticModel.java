package nz.ac.vuw.httpfuzz.jee;

import nz.ac.vuw.httpfuzz.EntryPoint;
import nz.ac.vuw.httpfuzz.Profile;
import nz.ac.vuw.httpfuzz.StaticModel;
import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.http.MethodSpec;
import nz.ac.vuw.httpfuzz.jee.doop.CallgraphBuilder;
import nz.ac.vuw.httpfuzz.jee.doop.PointstoBuilder;
import nz.ac.vuw.httpfuzz.jee.doop.StaticModelFromDoop;
import nz.ac.vuw.httpfuzz.jee.war.AdvancedWarScopeModel;
import nz.ac.vuw.httpfuzz.jee.war.SimpleWarScopeModel;
import nz.ac.vuw.httpfuzz.jee.war.StaticModelFromWar;
import nz.ac.vuw.httpfuzz.stan.callgraph.Callgraph;
import nz.ac.vuw.httpfuzz.stan.pointsto.Pointsto;
import java.io.File;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Composite pre-analysis model combining doop and war analysis.
 * @author jens dietrich
 */
public class JEEStaticModel implements StaticModel {

    private StaticModelFromWar warModel = null;

    public JEEStaticModel(File war,Predicate<String> isApplicationClass, Profile profile) throws Exception {
        this(war,new PointstoBuilder.Config(), new CallgraphBuilder.Config(), isApplicationClass,profile);
    }

    public JEEStaticModel(File war, PointstoBuilder.Config config1, CallgraphBuilder.Config config2, Predicate<String> isApplicationClass,Profile profile) throws Exception {
        warModel = new StaticModelFromWar(war,new AdvancedWarScopeModel(war,true,profile),profile);
//        Pointsto pointsto = doopPointsTo==null?null:new PointstoBuilder().build(doopPointsTo,config1);
//        Callgraph callgraph = doopCallgraph==null?null:new CallgraphBuilder().build(doopCallgraph,config2);
//        doopModel = new StaticModelFromDoop(callgraph,pointsto,isApplicationClass);
    }

    @Override
    public Set<String> getStringLiterals(EnumSet<Scope> scopes) {
        return warModel.getStringLiterals(scopes);
    }

    @Override
    public Set<Integer> getIntLiterals(EnumSet<Scope> scopes) {
        return warModel.getIntLiterals(scopes);
    }

    @Override
    public Set<String> getRequestParameterNames() {
        return warModel.getRequestParameterNames();
    }

    @Override
    public Set<String> getHeaderNames() {
        return warModel.getHeaderNames();
    }

    @Override
    public Set<EntryPoint> getEntryPoints() {
        return warModel.getEntryPoints();
    }

    @Override
    public Set<String> getEntryPointsURLPatterns() {
        return warModel.getEntryPointsURLPatterns();
    }

    @Override
    public Set<String> getReflectiveNames(EnumSet<Scope> scopes) {
        return warModel.getReflectiveNames(scopes);
    }

    @Override
    public Set<MethodSpec> getMethods() {
        return warModel.getMethods();
    }

    @Override
    public Set<Form> getForms() {
        return warModel.getForms();
    }
}
