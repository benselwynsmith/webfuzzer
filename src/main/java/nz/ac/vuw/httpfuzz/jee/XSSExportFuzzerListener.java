package nz.ac.vuw.httpfuzz.jee;

import nz.ac.vuw.httpfuzz.http.HeaderSpec;
import nz.ac.vuw.httpfuzz.http.ParameterSpec;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Exports detected XSS Vulnerabilities to json.
 * @author jens dietrich
 */
public class XSSExportFuzzerListener extends AbstractFuzzerListener  {

    private static final AtomicInteger counter = new AtomicInteger(0);
    private static final File folder = new File("logs");

    @Override
    public void fuzzingStarts() {
        super.fuzzingStarts();
        this.checkLogFolder(folder);
    }

    @Override
    public void newXSSDiscovered(RequestSpec requestSpec, String response, String token) {
        super.newXSSDiscovered(requestSpec, response, token);

        File file = new File(folder,"xss-" + counter.incrementAndGet() + ".json");
        JSONObject json = new JSONObject();


        try (FileWriter out = new FileWriter(file)) {

            // request
            JSONObject jRequest = new JSONObject();
            jRequest.put("method",requestSpec.getMethodSpec().name());
            jRequest.put("uri",requestSpec.getUriSpec().toURI());
            Map<String,String> params = new HashMap<>();
            for (ParameterSpec paramSpec:requestSpec.getParametersSpec().getParams()) {
                params.put(paramSpec.getKey().getValue(),paramSpec.getValue().getValue());
            }
            jRequest.put("parameters",params);
            Map<String,String> headers = new HashMap<>();
            for (HeaderSpec headerSpec:requestSpec.getHeadersSpec().getHeaders()) {
                headers.put(headerSpec.getKey().getValue(),headerSpec.getValue().getValue());
            }
            jRequest.put("headers",headers);
            json.put("request",jRequest);

            // response
            JSONObject jResponse = new JSONObject();
            jResponse.put("entity",response);
            json.put("response",jResponse);

            json.put("tainted-input",token);

            Set<Object> formOwnersUsed = requestSpec.getPropertyFromThisOrAncestor(RequestSpec.FORM_OWNER);
            JSONArray owners = new JSONArray();
            if (formOwnersUsed!=null) {
                for (Object formOwner:formOwnersUsed) {
                    // value should be string, usually there is only one unless the request has some crossover
                    owners.put(formOwner);
                }
            }
            json.put("known-form-owners",owners);

            json.write(out,1,3);
        } catch (Exception x) {
            Loggers.FUZZER.error("Error exporting XSS",x);
        }
    }
}
