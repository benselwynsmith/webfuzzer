package nz.ac.vuw.httpfuzz.jee.war;

import nz.ac.vuw.httpfuzz.Profile;
import nz.ac.vuw.httpfuzz.commons.LogSystem;
import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.html.HtmlParser;
import org.apache.log4j.Logger;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Collect html forms inside the war / jar.
 * @author jens dietrich
 */
public class HtmlFormExtractor {

    public static final Logger LOGGER = LogSystem.getLogger("analyse-war-extract-forms");

    public Set<Form> extractForms(File war, Profile profile) throws Exception {
        Set<Form> forms = new HashSet<>();

        new WarVisitor().visit(war,true,
            entry -> entry.getName().endsWith("html") || entry.getName().endsWith("htm"),
            (name,in) -> {
                try {
                    forms.addAll(HtmlParser.extractForms(profile,in,name));
                }
                catch (Exception x) {
                    LOGGER.warn("Error parsing form");
                }
            }
        );

        LOGGER.info("Extracted forms: " + forms.size());
        return forms;
    }

}
