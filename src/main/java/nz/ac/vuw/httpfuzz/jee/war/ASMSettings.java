package nz.ac.vuw.httpfuzz.jee.war;

import org.objectweb.asm.Opcodes;

/**
 * The ASM version used.
 * @author jens dietrich
 */
public class ASMSettings {
    public static final int VERSION = Opcodes.ASM8;
}
