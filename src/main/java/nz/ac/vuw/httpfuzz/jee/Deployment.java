package nz.ac.vuw.httpfuzz.jee;

import javax.annotation.Nullable;
import java.io.File;

/**
 * Abstraction for the deployment of a fuzzing campaing on some server.
 * @author jens dietrich
 */
public interface Deployment {

    /**
     * Deploy a war file, including instrumentation.
     * Instances should hold enough state about the deployment to undeploy.
     * This implies that for each deployment a unique instance should be used,
     * this can be controlled by throwing IllegalStateExceptions in deploy.
     * @param war
     * @param aspectJJar
     * @param contextName
     * @return true if deployment was successful, false otherwise
     * @throws Exception
     */
    boolean deploy(File war, File aspectJJar,String contextName) throws Exception;

    /**
     * Undeploy the previously deployed web application.
     */
    void undeploy() throws Exception;

    String getHost();

    int getPort();

    String getURL();

    /**
     * Callback that can be used to check and maintain server health.
     * E.g. count requests, and perform some task after X requests.
     * @return a listener, or null
     */
    @Nullable  FuzzerListener getMaintenanceListener ();

}
