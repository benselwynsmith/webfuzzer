package nz.ac.vuw.httpfuzz.jee.war;

import nz.ac.vuw.httpfuzz.commons.LogSystem;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Utility to recursively traverse war files, i.e. visits resources in the war, and within the nested jars.
 * Also works for nested spring mvc / boot jars.
 * @author jens dietrich
 */
public class WarVisitor {

    public static final Logger LOGGER = LogSystem.getLogger(WarVisitor.class);
    private boolean copyStreams = true;

    public WarVisitor(boolean copyStreams) {
        this.copyStreams = copyStreams;
    }

    public WarVisitor() {
        this(true);
    }

    public void visit (File war, boolean visitNestedJars, Predicate<ZipEntry> filter, BiConsumer<String,InputStream> action) throws Exception {
        ZipFile zip = new ZipFile(war);
        Enumeration<? extends ZipEntry> en = zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (filter.test(e)) {
                    InputStream in = zip.getInputStream(e);
                    action.accept(name,in);
                    in.close();
            }
            if (visitNestedJars && (name.startsWith("WEB-INF/lib/") || name.startsWith("BOOT-INF/lib/")) && name.endsWith(".jar")) {
                InputStream in = zip.getInputStream(e);
                ZipInputStream in2 = new ZipInputStream(in);
                ZipEntry e2 = null;
                try {
                    while ((e2 = in2.getNextEntry()) != null) {
                        String name2 = e2.getName();
                        if (filter.test(e2)) {
                            action.accept(name+"!/"+name2, copyStream(in2));
                        }
                    }
                }
                catch (Exception x) {
                    LOGGER.warn("Error extracting  data from nested jar: " + war.getAbsolutePath() + "!/" + name,x);
                }

                in2.close();
            }
        }
    }

    private InputStream copyStream(InputStream in) throws IOException {
        if (this.copyStreams) {
            // useful in situations when the consumer may close the stream
            byte[] data = IOUtils.toByteArray(in);
            return new ByteArrayInputStream(data);
        }
        else {
            return in;
        }
    }


}
