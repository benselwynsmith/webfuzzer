package nz.ac.vuw.httpfuzz.jee;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.googlecode.concurrenttrees.common.CharSequences;
import com.googlecode.concurrenttrees.radix.node.concrete.DefaultCharSequenceNodeFactory;
import com.googlecode.concurrenttrees.solver.LCSubstringSolver;
import nz.ac.vuw.httpfuzz.*;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import nz.ac.vuw.httpfuzz.feedback.FeedbackParser;
import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.html.HtmlParser;
import nz.ac.vuw.httpfuzz.http.*;
import org.apache.commons.cli.CommandLine;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.PropertyConfigurator;
import org.json.JSONObject;
import java.io.*;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Base class to implement fuzzers.
 * @author jens dietrich
 */
public abstract class AbstractFuzzer  {

    public static final int DEFAULT_TRIALS = Integer.MAX_VALUE;
    public static final int THREADS = 1;

    protected int trials = DEFAULT_TRIALS;
    protected AtomicBoolean ACTIVE = new AtomicBoolean(true);
    protected boolean httpKeepAlive = false;
    protected PoolingHttpClientConnectionManager poolingmgr;
    protected History history = new DefaultHistory();
    protected SourceOfRandomness sourceOfRandomness = new SourceOfRandomness();
    protected BasicCookieStore cookieStore = new BasicCookieStore();
    protected static int logProgressInterval = 100;
    protected String authAddress = "";
    // protected List<String> authToken;
    protected long startTime = -1;

    protected Collection<FuzzerListener> listeners = new ArrayList<>();


    public static class HttpRecord {
        String ticketId = null;
        RequestSpec requestSpec = null;
        HttpRequest request = null;
        HttpResponse response = null;

        public HttpRecord(String ticketId, RequestSpec requestSpec, HttpRequest request, HttpResponse response) {
            this.ticketId = ticketId;
            this.requestSpec = requestSpec;
            this.request = request;
            this.response = response;
        }
    }

    public AbstractFuzzer() {

        Loggers.FUZZER.info("Initialising connection manager");
        poolingmgr = new PoolingHttpClientConnectionManager(); // use the same connection mgr for all requests
        poolingmgr.setMaxTotal(100);
        poolingmgr.setDefaultMaxPerRoute(50);

    }


    public void fuzz(CommandLine cmd) throws Exception {
        this.init(cmd);
        this.deploy(cmd);
        if (isServerReady()) {
            this.authenticate(cmd);
            this.run(cmd);
        }
        this.undeploy(cmd);
        this.tearDown();
    }

    private boolean isServerReady() throws Exception {
        Loggers.FUZZER.info("Waiting for web application (up to a minute)");
        int DELAY = 5;
        int i = 0;
        int MAX_CHECKS = 30;
        HttpClientBuilder builder = HttpClientBuilder.create();
        CloseableHttpClient httpClient = builder.build();

        while (true) {

            String url = Context.getDefault().getProfile().getWebappRootURL();
            HttpGet httpGet = new HttpGet(url);

            try(CloseableHttpResponse response =httpClient.execute(httpGet);) {
                if (i++ > MAX_CHECKS) {
                    Loggers.FUZZER.error("Could not detect service at: " + url);
                    return false;
                }
                if (response.getStatusLine().getStatusCode() >= 500) { // catch 503 (service not available).
                    Loggers.FUZZER.error("Waiting for service at: " + url);
                    TimeUnit.SECONDS.sleep(DELAY);
                    continue;
                }
                else
                    return true;
            } catch (Exception e) {
                TimeUnit.SECONDS.sleep(DELAY);
            }

        }
    }


    /**
     * Initialise fuzzer from commandline parameters.
     * @param cmd the parsed commandline parameters
     */
    protected void init(CommandLine cmd) throws Exception {

        // cullling old logs
        File LOG_FOLDER = new File("logs");
        if (LOG_FOLDER.exists()) {
            System.out.println("Culling log files in " + LOG_FOLDER.getAbsolutePath()); // log system is not yet running !
            for (File f : LOG_FOLDER.listFiles(f -> f.getName().endsWith(".log"))) {
                f.delete();
            }
        }

        PropertyConfigurator.configure("log4j.properties");

        if (cmd.hasOption("trials")) {
            try {
                String trialsTxt = cmd.getOptionValue("trials");
                this.trials = Integer.parseInt(trialsTxt);
                Loggers.FUZZER.info("Setting trials to: " + trials);
            }
            catch (NumberFormatException x) {
                Loggers.FUZZER.warn("Cannot parse trials value, will use default: " + trials);
            }
        }

        if (cmd.hasOption("logInterval")) {
            try {
                String logIntervalTxt = cmd.getOptionValue("logInterval");
                this.logProgressInterval = Integer.parseInt(logIntervalTxt);
                Loggers.FUZZER.info("Setting logProgressInterval to: " + logProgressInterval);
            }
            catch (NumberFormatException x) {
                Loggers.FUZZER.warn("Cannot parse log interval value, will use default: " + logProgressInterval);
            }
        }

        if (cmd.hasOption("randomseed")) {
            int randomSeed = Integer.parseInt(cmd.getOptionValue("randomseed"));
            Loggers.FUZZER.info("Setting seed to: " + randomSeed);
            this.sourceOfRandomness.setSeed(randomSeed);
        }

        this.httpKeepAlive = cmd.hasOption("httpKeepAlive") && cmd.getOptionValue("httpKeepAlive").equals("true");
        Loggers.FUZZER.warn("HTTP persistent connections set to : " + httpKeepAlive);


    }

    protected void authenticate(CommandLine cmd) throws Exception {
        // authentication
        // Pre Request (Auth)
        if (cmd.hasOption("authAddress")) {
            //URL for login request (assuming POST for now)
            this.authAddress = cmd.getOptionValue("authAddress");
            Loggers.FUZZER.info("authAddress: " + this.authAddress);

            //Sanity checks
            if (cmd.hasOption("authParams")) {
                Loggers.FUZZER.info("authParams: " + cmd.getOptionValue("authParams"));
            } else {
                Loggers.FUZZER.error("Missing authParams for authentication.");
                System.exit(1);
            }

            //Request parameters to be added to POST
            List<BasicNameValuePair> authParams = new ArrayList<BasicNameValuePair>();
            if (cmd.hasOption("authParams")) {
                String s = cmd.getOptionValue("authParams");
                String[] split = s.split(",");
                for (String kv : split) {
                    String[] split2 = kv.split("=");
                    authParams.add(new BasicNameValuePair(split2[0], split2[1]));
                }
            }

            //Create request
            Profile profile = Context.getDefault().getProfile();
            URIBuilder uriBuilder = new URIBuilder()
                    .setScheme("http")
                    .setHost(profile.getHost())
                    .setPort(profile.getPort());
            String path = profile.getPathFirstTokens() == null ? "" : profile.getPathFirstTokens().stream().collect(Collectors.joining("/"));
            uriBuilder.setPath(path + this.authAddress);
            URI uri = uriBuilder.build();
            HttpPost httpPost = new HttpPost(uri);

            //Add Parameters to request and send
            httpPost.setEntity(new UrlEncodedFormEntity(authParams, "UTF-8"));

            // TODO is server up ?  -- this is timing sensitive
            Loggers.FUZZER.info("Sending authentication request: " + httpPost);

            HttpResponse response = sendRequest(httpPost);
            Loggers.FUZZER.info("Authentication request returned " + (response==null?"null":response.getStatusLine()));

            if (response!=null) {
                // the Location header often provides an indication of whether authentication was successful
                // e.g. it might be an error or login page if authentication fails, so lets log this
                Header[] headers = response.getHeaders("Location");
                if (headers!=null) {
                    for (Header locationHeader:headers) {
                        String value = locationHeader.getValue();
                        String msg = "Authentication request returned location header: " + value;
                        if (value.toLowerCase().contains("login") || value.toLowerCase().contains("error")) {
                            Loggers.FUZZER.warn(msg);
                        }
                        else {
                            Loggers.FUZZER.info(msg);
                        }
                    }
                }
            }

            if (response!=null && response.getStatusLine().getStatusCode()==503) {
                Loggers.FUZZER.info("Authentication request returned 503, server might not yet be available, will wait and restart authentication");
                try {
                    Thread.sleep(5000);
                    authenticate(cmd);
                }
                catch (InterruptedException x){}
            }

        }
    }

    protected void run(CommandLine cmd) throws Exception {

        RequestSpecGenerator generator = new RequestSpecGenerator();
        this.startTime = System.currentTimeMillis();
        AtomicInteger threadCount = new AtomicInteger();

        ExecutorService threadpool = new ThreadPoolExecutor(
                THREADS,THREADS,
                1,TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(1_000),
                runnable -> new Thread(runnable,"webfuzz-client-" + threadCount.incrementAndGet()),
                new ThreadPoolExecutor.CallerRunsPolicy()
        );
        List<JEEFuzzer.HttpRecord> buffer = Collections.synchronizedList(new LinkedList<>());

        Cache<String,RequestSpec> requestSpecsByTicketId = CacheBuilder.newBuilder()
                .maximumSize(50_000)
                .expireAfterWrite(60, TimeUnit.MINUTES)
                .build();

        this.listeners.forEach(l -> l.fuzzingStarts());
        final AtomicInteger counter = new AtomicInteger();

        for (int i=0;i<this.trials;i++) {
            threadpool.submit(() -> {
                try {
//                    if (counter.incrementAndGet()%1_000 == 0) {
//                        Loggers.FUZZER.info("Initiating re-authentication");
//                        this.authenticate(cmd);
//                    }
//                    else {
                        this.sendRequest(generator.apply(this.sourceOfRandomness, Context.getDefault()), this.history, buffer, requestSpecsByTicketId);
//                    }
                }
                catch (Exception x) {
                    Loggers.FUZZER.debug("Request failed",x);
                }
            });
        }

        threadpool.shutdown();
        threadpool.awaitTermination(1, TimeUnit.DAYS);

    }

    protected abstract void deploy(CommandLine cmd) throws Exception;

    protected abstract void undeploy(CommandLine cmd) throws Exception;

    protected abstract void tearDown() throws Exception;

    protected CloseableHttpResponse sendRequest(HttpUriRequest request, int timeBetweenAttempts, int maxAttempts,int attemptCounter) throws IOException {

        if (attemptCounter>=maxAttempts) {
            return null;
        }

        if (!httpKeepAlive) {
            request.setHeader("Connection", "close");
        }
        CloseableHttpResponse response = null;
        if (!ACTIVE.get()) return null;
        try {
            CloseableHttpClient httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).setConnectionManager(poolingmgr).build();

            response = httpClient.execute(request);
            return response;
        }
        catch (SocketException x) {
            if (maxAttempts>1) {
                if (attemptCounter == 0) {
                    Loggers.FUZZER.warn("Network problem, resending request " + request.getMethod() + " " + request.getURI());
                }
                // x.printStackTrace();
                try {
                    Thread.sleep(timeBetweenAttempts);
                } catch (InterruptedException e) {
                    Loggers.FUZZER.error("Exception: " + x.getMessage(), x);
                }
                if (response != null) {
                    response.close();
                }
                return sendRequest(request, timeBetweenAttempts, maxAttempts, attemptCounter + 1);
            }
            return null;
        }
    }


    protected CloseableHttpResponse sendRequest(HttpUriRequest request, int timeBetweenAttempts, int maxAttempts) throws IOException {
        return sendRequest(request, timeBetweenAttempts,maxAttempts,0);
    }


    protected CloseableHttpResponse sendRequest(HttpUriRequest request) throws IOException {
        return sendRequest(request, 100,1);
    }

    // class to avoid parsing the response entity twice
    private class ResponseWrapper {
        private CloseableHttpResponse response = null;
        private String content = null;

        ResponseWrapper(CloseableHttpResponse response) {
            this.response = response;
        }

        String getContent() {
            if (content == null) {
                try {
                    this.content = EntityUtils.toString(response.getEntity());
                }
                catch (IOException x) {
                    Loggers.FUZZER.warn("Error parsing response entity",x);
                }
            }
            return content;
        }
    }

    /**
     * Send a request generated from a request spec.
     * @param requestSpec
     * @param history
     */
    public void sendRequest(RequestSpec requestSpec,History history,List<HttpRecord> buffer,Cache<String,RequestSpec> requestSpecsByTicketId) {
        Context context = Context.getDefault();
        HttpUriRequest request = null;
        try {
            request = requestSpec.toRequest();
        } catch (URISyntaxException e) {
            processFeedback(buffer,requestSpecsByTicketId);
            return;
        }
        // Append auth tokens
        // NOTE: this is now done by using the cookie store.  Will not work if server does not use cookies (but for instance, URL rewritting - this is usually only used as backup if client does not support cookies)
//        if (authToken != null) {
//            request.setHeader("Cookie: ",String.join("; ",authToken));
//        }

        if (history.isNew(requestSpec)) {
            // Loggers.FUZZER.info("next request: " + request);
            try (CloseableHttpResponse response = sendRequest(request);) {
                ResponseWrapper responseWrapper = new ResponseWrapper(response);
                if (response == null)
                    return; // session cancelled

                listeners.forEach(l -> l.requestSent(requestSpec, response.getStatusLine().getStatusCode()));

                // if this is html, parse response for forms
                Header contentTypeHeader = response.getFirstHeader("Content-Type");

                if (contentTypeHeader != null && (contentTypeHeader.getValue().startsWith("text/html") || contentTypeHeader.getValue().startsWith("application/xhtml+xml"))) {
                    try {
                        Collection<Form> forms = HtmlParser.extractForms(context.getProfile(), responseWrapper.getContent(), request.getURI().toString());
                        if (forms != null && forms.size() > 0) {
                            Set<Form> newForms = context.getDynamicModel().formsRecorded(requestSpec, request, response, forms);
                            listeners.forEach(l -> l.newFormsDiscovered(requestSpec,newForms));
                        }
                    } catch (Exception x) {
                        // LOGGER.warn("Exception parsing HTML form returned by response");
                    }
                }

                // pickup and process feedback
                Header ticketIdHeader = response.getFirstHeader(RTConstants.FUZZING_FEEDBACK_TICKET_HEADER);

                // START DEBUGGING CODE
//                if (!request.getURI().toString().contains("__fuzzing")) {
//                    LOGGER.info("request " + request + "\tgenerated by " + requestSpec.getGenerator().name());
//                    LOGGER.info("\t" + response.getStatusLine());
//                    LOGGER.info("\tticket: #" + ticketIdHeader);
//                }
                // END DEBUGGING CODE

                if (ticketIdHeader == null) {
                    // Loggers.FUZZER.info("No feedback header returned for " + request);
                    // there is an issue that the header is not set when a server error (500) occurs
                    if (response.getStatusLine().getStatusCode() >= 500) {
                        Loggers.FUZZER.info("Response code 500 encountered: " + requestSpec);
                        Loggers.RESULTS_500.info("Response code 500 encountered: " + requestSpec);
                        context.getDynamicModel().executionsRecorded(requestSpec, request, response, Collections.EMPTY_LIST);

                    }
                } else {
                    // register record
                    HttpRecord record = new HttpRecord(ticketIdHeader.getValue(), requestSpec, request, response);
                    buffer.add(record);
                    requestSpecsByTicketId.put(ticketIdHeader.getValue(), requestSpec);
                    history.keep(requestSpec);

                    checkForXSS(record, responseWrapper.getContent());
                }
            } catch (Exception x) {
                try {
                    Loggers.FUZZER.debug("Error sending request " + x.getMessage() + " (change log level to DEBUG for details)");
                    Loggers.FUZZER.debug("Error sending request " + requestSpec.toRequest());
                    Loggers.FUZZER.debug("\theaders:");
                    for (HeaderSpec headerSpec : requestSpec.getHeadersSpec().getHeaders()) {
                        Loggers.FUZZER.debug("\t\t" + headerSpec);
                    }
                } catch (URISyntaxException xx) {
                    Loggers.FUZZER.debug("Error sending request " + requestSpec);
                }
                Loggers.FUZZER.debug("Details: " + x.getMessage(), x);
            }
        }
        else {
            listeners.forEach(l -> l.requestRejected(requestSpec));
        }

        processFeedback(buffer,requestSpecsByTicketId);

    }

    protected void processFeedback(List<HttpRecord> buffer, Cache<String, RequestSpec> requestSpecsByTicketId) {
        /**
         * The reason to fetch the feedback for older requests is simply that there are sometimes issues when requesting
         * feedback directly after the original request leads to 404 due to concurrency issues on the serve r
         * waiting while issuing new requests in the meantime may help to avoid performance bottlenecks caused by this.
         */
        if (buffer.size() < 100)
            return;
        Context context = Context.getDefault();
        HttpRecord record = buffer.remove(0);

        Profile profile = context.getProfile();
        URIBuilder uriBuilder = new URIBuilder()
                .setScheme("http")
                .setHost(profile.getHost())
                .setPort(profile.getPort());

        RequestSpec originalRequestSpec = requestSpecsByTicketId.getIfPresent(record.ticketId);
        requestSpecsByTicketId.invalidate(record.ticketId);

        String path = profile.getPathFirstTokens() == null ? "" : profile.getPathFirstTokens().stream().collect(Collectors.joining("/"));
        path = path + RTConstants.FUZZING_FEEDBACK_PATH_TOKEN + '/' + record.ticketId;


        uriBuilder.setPath(path);
        URI uri = null;

        try {
            uri = uriBuilder.build();
        } catch (URISyntaxException e) {
            return;
        }
        Loggers.FUZZER.debug("feedback request: " + uri);
        HttpUriRequest feedbackRequest = new HttpGet(uri);

        try (CloseableHttpResponse feedbackResponse = sendRequest(feedbackRequest);) {

            listeners.forEach(l -> l.feedbackRequestSent(record.ticketId,feedbackResponse.getStatusLine().getStatusCode()));
            // pick up dynamic callgraph information

            if (feedbackResponse == null)
                return; // session cancelled !
            assert feedbackResponse.getFirstHeader("Content-Type").getValue().startsWith("application/json");
            assert feedbackResponse.getStatusLine().getStatusCode() == 200;

            Set<ExecutionPoint> invocations = new HashSet<>();
            Set<String> requestParameterNames = new HashSet<>();
            Set<String> stacktracesForCriticalMethods = new HashSet<>();

            if (feedbackResponse.getStatusLine().getStatusCode() == 200) {
                String data = EntityUtils.toString(feedbackResponse.getEntity());

                JSONObject json = new JSONObject(data);
                invocations = FeedbackParser.extractMethodSpecs(json);
                requestParameterNames = FeedbackParser.extractRequestParameterNames(json);
                stacktracesForCriticalMethods = FeedbackParser.extractStackTracesForUnsafeMethods(json);


            } else {
                Loggers.FUZZER.debug("Feedback request to " + uri + " has failed, response status: " + feedbackResponse.getStatusLine());
            }

            Set<ExecutionPoint> newlyDiscoveredExecutionPoints = context.getDynamicModel().executionsRecorded(originalRequestSpec, record.request, record.response, invocations);
            listeners.forEach(l -> l.newExecutionPointsDiscovered(originalRequestSpec,newlyDiscoveredExecutionPoints));

            Set<String> newlyDiscoveredRequestParameterNames = context.getDynamicModel().requestParametersRecorded(originalRequestSpec, record.request, record.response, requestParameterNames);
            listeners.forEach(l -> l.newRequestParameterNamesDiscovered(originalRequestSpec,newlyDiscoveredRequestParameterNames));

            for (String stackTrace : stacktracesForCriticalMethods) {
                if (context.getDynamicModel().stacktraceToCriticalMethodRecorded(originalRequestSpec, record.request, record.response, stackTrace)) {
                    listeners.forEach(l -> l.newInvocationsOfCriticalMethodDiscovered(originalRequestSpec,stackTrace));
                }
            }

        } catch (IOException x) {
            Loggers.FUZZER.error("Exception: " + x.getMessage(), x);
        }
    }


    private void checkForXSS(HttpRecord record,String responseEntityContent) {
        // only check normal responses by default
        // TODO: could clients also build sites enabling XSS from error data ?
        if (record.response.getStatusLine().getStatusCode() >= 400) return;

        try {
            Multimap<String,RequestSpec.RequestDataKind> requestData = SpecProvenanceUtils.extractTaintedParts(record.requestSpec);

            for (String token:requestData.keySet()) {
                //very simple string matching -- TODO do something more sophisticated like longest substring, edit distance etc
                // ignore very short strings
                if (responseEntityContent.contains(token)) { // ignore very short tokens
                    listeners.forEach(l -> l.newXSSDiscovered(record.requestSpec,responseEntityContent,token));
                }
            }
        }
        catch (Exception x) {
            Loggers.FUZZER.info("Error processing http transactions to scan for XSS vulnerabilities",x );
        }
    }

    // TODO -- integrate and refactor
    private void checkForTaintFlowToSink(HttpRecord record, List<List> sinkStrArgs) {

        if (sinkStrArgs.isEmpty())
            return;

        Similarity subStrSimilarity = new Similarity() {
            @Override
            public boolean apply(String src, String tgt) {
                if (src.contains(tgt) || tgt.contains(src))
                    return true;
                else
                    return false;
            }
        };
        Similarity lcsSimilarity = new Similarity() {
            @Override
            public boolean apply(String src, String tgt) {
                LCSubstringSolver solver = new LCSubstringSolver(new DefaultCharSequenceNodeFactory());
                solver.add(src);
                solver.add(tgt);
                String longestCommonSubstring = CharSequences.toString(solver.getLongestCommonSubstring());
                if (longestCommonSubstring.length() >= THRESHOLD) {
                    return true;
                }
                return false;
            }
        };

        if (record.response.getStatusLine().getStatusCode() >= 400) return;

        try {
            Multimap<String,RequestSpec.RequestDataKind> requestData = SpecProvenanceUtils.extractTaintedParts(record.requestSpec);

            List<String> argsList = sinkStrArgs.get(1);
            List<String> paramsList = sinkStrArgs.get(0);
            Set<String> tokens = requestData.keySet().stream()
                    .filter( i -> i.length() >= Similarity.THRESHOLD).collect(Collectors.toSet());
            Set<String> args = argsList.stream()
                    .filter( i -> i.length() >= Similarity.THRESHOLD).collect(Collectors.toSet());

            Set<List<String>> pairs = Sets.cartesianProduct(tokens, args);
            pairs.stream().filter(
                    pair -> {
                        String token = pair.get(0);
                        String arg = pair.get(1);
                        if (subStrSimilarity.apply(token, arg)) {
                           // log taint
                            return true;
                        } else {
                            return false;
                        }
                    } ).collect(Collectors.toList());

        }
        catch (Exception x) {
            Loggers.FUZZER.info("Error processing http transactions to scan for taint vulnerabilities",x );
        }
    }

    static interface Similarity {
        int THRESHOLD = 5;
        boolean apply(String source, String target);
    }

    public Collection<FuzzerListener> getListeners() {
        return Collections.unmodifiableCollection(listeners);
    }

    public boolean addListener(FuzzerListener listener) {
        boolean success = this.listeners.add(listener);
        if (success) {
            Loggers.FUZZER.info("Installed listener: " + listener);
        }
        else {
            Loggers.FUZZER.warn("Failed to install listener: " + listener);
        }
        return success;
    }

    public boolean removeListener(FuzzerListener listener) {
        boolean success = this.listeners.remove(listener);
        if (success) {
            Loggers.FUZZER.info("Uninstalled listener: " + listener);
        }
        else {
            Loggers.FUZZER.warn("Failed to uninstall listener: " + listener);
        }
        return success;
    }
}
