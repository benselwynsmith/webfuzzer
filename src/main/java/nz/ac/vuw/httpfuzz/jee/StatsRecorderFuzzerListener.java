package nz.ac.vuw.httpfuzz.jee;

import nz.ac.vuw.httpfuzz.ExecutionPoint;
import nz.ac.vuw.httpfuzz.commons.LogSystem;
import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Records performance stats.
 * @author jens dietrich
 */
public class StatsRecorderFuzzerListener extends AbstractFuzzerListener  {

    private static final Logger LOGGER = LogSystem.getLogger("stats-service");
    private static final String CSV_SEP = "\t";
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd - HH:mm:ss");

    private enum Key {REQUESTS_SENT,REQUESTS_SENT_200,REQUESTS_SENT_300,REQUESTS_SENT_400,REQUESTS_SENT_500,REQUESTS_REJECTED,FEEDBACK_REQUESTS_SENT,NEW_METHODS_DISCOVERED, NEW_STACKTRACE_TO_CRITICAL_SINK_DISCOVERED, NEW_XSS_DISCOVERED, NEW_FORMS_DISCOVERED, NEW_REQUEST_PARAMETER_NAMES_DISCOVERED}
    private Map<Key,Integer> counts = new ConcurrentHashMap<>();
    private boolean fileInitialised = false;
    private File logFolder = new File("logs");
    private File statsFile = new File(logFolder,"stats.csv");
    private long delay = 0;

    private int snapshotInterval = 60_000;

    public StatsRecorderFuzzerListener(int snapshotInterval) {
        this.snapshotInterval = snapshotInterval;
    }

    @Override
    public void fuzzingStarts() {

        this.checkLogFolder(logFolder);
        Runnable task = () -> {
            while (true) {

                try {
                    assert snapshotInterval > delay;
                    Thread.sleep(snapshotInterval-delay);

                    long t1 = System.currentTimeMillis();
                    // snapshot
                    Map<Key,Integer> snapshot = new HashMap<>();
                    synchronized (StatsRecorderFuzzerListener.this) {
                        snapshot.putAll(counts);
                        counts.clear();
                    }

                    try (PrintWriter writer = new PrintWriter(new FileWriter(statsFile,fileInitialised))) {
                        if (!fileInitialised) {
                            String headerLine = Stream.of(Key.values())
                                .map(k -> k.name())
                                .map(n -> n.replace('_','-').toLowerCase())
                                .collect(Collectors.joining(CSV_SEP));
                            writer.print(headerLine);
                              writer.print(CSV_SEP);
                              writer.print("timestamp");
                            writer.println();
                        }

                        String nextRow = Stream.of(Key.values())
                            .map(k -> snapshot.computeIfAbsent(k,k2 -> 0))
                            .map(i -> i.toString())
                            .collect(Collectors.joining(CSV_SEP));
                        writer.print(nextRow);
                        writer.print(CSV_SEP);
                        writer.print(DATE_FORMAT.format(new Date()));
                        writer.println();


                        LOGGER.info("Stats data appended to " + statsFile.getAbsolutePath());

                    } catch (IOException e) {
                        LOGGER.error("Error in stats service", e);
                    }
                    fileInitialised = true;
                    long t2 = System.currentTimeMillis();
                    delay = t2-t1;

                } catch (InterruptedException e) {
                    LOGGER.error("Error in stats service", e);
                }
            }

        };
        new Thread(task,"fuzzer-stats-service").start();
        LOGGER.info("Stats service started");
    }



    @Override
    public synchronized void requestRejected(RequestSpec request) {
        counts.compute(Key.REQUESTS_REJECTED,(k,v) -> v==null?1:v+1);
    }

    @Override
    public synchronized void requestSent(RequestSpec request, int statusCode) {
        counts.compute(Key.REQUESTS_SENT,(k,v) -> v==null?1:v+1);

        if (statusCode>=200 && statusCode<300) {
            counts.compute(Key.REQUESTS_SENT_200,(k,v) -> v==null?1:v+1);
        }
        else if (statusCode>=300 && statusCode<400) {
            counts.compute(Key.REQUESTS_SENT_300,(k,v) -> v==null?1:v+1);
        }
        else if (statusCode>=400 && statusCode<500) {
            counts.compute(Key.REQUESTS_SENT_400,(k,v) -> v==null?1:v+1);
        }
        else if (statusCode>=500 && statusCode<600) {
            counts.compute(Key.REQUESTS_SENT_500,(k,v) -> v==null?1:v+1);
        }

        else {
            LOGGER.warn("request encountered with unknown status code: " + statusCode);
        }

    }

    @Override
    public synchronized void feedbackRequestSent(String ticketId, int statusCode) {
        counts.compute(Key.FEEDBACK_REQUESTS_SENT,(k,v) -> v==null?1:v+1);
    }

    @Override
    public synchronized void newExecutionPointsDiscovered(RequestSpec requestSpec, Set<ExecutionPoint> executionPoints) {
        int count = executionPoints.size();
        if (count>0) {
            counts.compute(Key.NEW_METHODS_DISCOVERED,(k,v) -> v==null?count:v+count);
        }
    }

    @Override
    public synchronized void newRequestParameterNamesDiscovered(RequestSpec requestSpec, Set<String> requestParameterNames) {
        int count = requestParameterNames.size();
        if (count>0) {
            counts.compute(Key.NEW_REQUEST_PARAMETER_NAMES_DISCOVERED,(k,v) -> v==null?count:v+count);
        }
    }

    @Override
    public synchronized void newInvocationsOfCriticalMethodDiscovered(RequestSpec requestSpec, String stackTrace) {
        counts.compute(Key.NEW_STACKTRACE_TO_CRITICAL_SINK_DISCOVERED,(k,v) -> v==null?1:v+1);
    }

    @Override
    public synchronized void newXSSDiscovered(RequestSpec requestSpec, String response, String token) {
        counts.compute(Key.NEW_XSS_DISCOVERED,(k,v) -> v==null?1:v+1);
    }

    @Override
    public synchronized void newFormsDiscovered(RequestSpec requestSpec, Set<Form> forms) {
        int count = forms.size();
        if (count>0) {
            counts.compute(Key.NEW_FORMS_DISCOVERED,(k,v) -> v==null?count:v+count);
        }
    }


    @Override
    public String toString() {
        return "StatsRecorderFuzzerListener{" + "snapshotInterval=" + snapshotInterval + " ms}";
    }
}
