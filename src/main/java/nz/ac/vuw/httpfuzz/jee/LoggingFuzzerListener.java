package nz.ac.vuw.httpfuzz.jee;

import nz.ac.vuw.httpfuzz.ExecutionPoint;
import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.html.Input;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import org.apache.log4j.Logger;
import java.util.Collection;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

/**
 * Listener that logs events.
 * @author jens dietrich
 */
public class LoggingFuzzerListener implements FuzzerListener  {

    private AtomicLong time = new AtomicLong(System.currentTimeMillis());
    private AtomicInteger sentRequestCount = new AtomicInteger();
    private AtomicInteger rejectedRequestCount = new AtomicInteger();
    private int logProgressInterval = 100;

    public LoggingFuzzerListener(int logProgressInterval) {
        this.logProgressInterval = logProgressInterval;
    }

    @Override
    public void fuzzingStarts() {}

    @Override
    public void requestRejected(RequestSpec request) {
        rejectedRequestCount.incrementAndGet();
    }

    @Override
    public void requestSent(RequestSpec request, int statusCode) {
        int c = sentRequestCount.incrementAndGet();
        if (c % logProgressInterval == 0) {
            long now = System.currentTimeMillis();
            Loggers.FUZZER.info("Request #" + c + " , time: " + (now - time.get()) + " ms " + " ,  rejected (duplicated) requests: " + rejectedRequestCount);
            time.set(now);
        }
    }

    @Override
    public void feedbackRequestSent(String ticketId, int statusCode) {

    }

    @Override
    public void newExecutionPointsDiscovered(RequestSpec requestSpec, Set<ExecutionPoint> executionPoints) {
        if (!executionPoints.isEmpty()) {
            Loggers.FUZZER.info("New execution points encountered for " + requestSpec + " (see log files for full details)");
            logDetails(Loggers.FUZZER,executionPoints,3);
            Loggers.COVERAGE_METHODS.info("New execution points encountered for " + requestSpec);
            logDetails(Loggers.COVERAGE_METHODS,executionPoints,Integer.MAX_VALUE);
            Loggers.COVERAGE_METHODS.info("");
        }
    }

    @Override
    public void newRequestParameterNamesDiscovered(RequestSpec requestSpec, Set<String> requestParameterNames) {
        if (!requestParameterNames.isEmpty()) {
            Loggers.FUZZER.info("New request parameters encountered for " + requestSpec + " (see log files for full details)");
            logDetails(Loggers.FUZZER,requestParameterNames,3);
            Loggers.COVERAGE_REQUEST_PARAMETERS.info("New request parameters encountered for " + requestSpec);
            logDetails(Loggers.COVERAGE_REQUEST_PARAMETERS,requestParameterNames,Integer.MAX_VALUE);
            Loggers.COVERAGE_REQUEST_PARAMETERS.info("");
        }
    }

    @Override
    public void newInvocationsOfCriticalMethodDiscovered(RequestSpec requestSpec, String stackTrace) {
        Loggers.FUZZER.info("New invocation of critical method encountered for " + requestSpec + " (see log files for full stacktraces");
        logDetails(Loggers.FUZZER,stackTrace.split("\n"),5);
        Loggers.RESULTS_SINK_METHODS.info("New invocation of critical method encountered for " + requestSpec + " (see log files for full stacktraces");
        logDetails(Loggers.RESULTS_SINK_METHODS,stackTrace.split("\n"),Integer.MAX_VALUE);
        Loggers.RESULTS_SINK_METHODS.info("");
    }

    @Override
    public void newXSSDiscovered(RequestSpec requestSpec, String response, String token) {
        Loggers.FUZZER.info("Potential XSS vulnerability encountered for " + requestSpec + " (see logs for details)");
        Loggers.RESULTS_XSS.info("Potential XSS vulnerability encountered");
        Loggers.RESULTS_XSS.info("\trequest is: " + requestSpec);
        Loggers.RESULTS_XSS.info("\treplicated token is " + token);
        Loggers.RESULTS_XSS.info("\trequest generation provenance " + requestSpec.getSources());

        Set<Object> formOwnersUsed = requestSpec.getPropertyFromThisOrAncestor(RequestSpec.FORM_OWNER);
        if (formOwnersUsed!=null) {
            Loggers.RESULTS_XSS.info("\trequest was generated from form");
            for (Object formOwner:formOwnersUsed) {
                // value should be string, usually there is only one unless the request has some crossover
                Loggers.RESULTS_XSS.info("\tform owner: " + formOwner);
            }
        }
    }

    @Override
    public void newFormsDiscovered(RequestSpec requestSpec, Set<Form> forms) {
        if (!forms.isEmpty()) {
            Loggers.FUZZER.debug(forms.size() + " form(s) recorded: " + requestSpec);
            Loggers.COVERAGE_FORMS.info("Form(s) recorded: " + requestSpec);
            int count = 0;
            for (Form form : forms) {
                Loggers.COVERAGE_FORMS.info("form " + ++count);
                Loggers.COVERAGE_FORMS.info("\tmethod: " + form.getMethod());
                Loggers.COVERAGE_FORMS.info("\taction: " + form.getAction());
                for (Input input : form.getInputs()) {
                    Loggers.COVERAGE_FORMS.info("\tinput: " + input);
                }
            }
            Loggers.COVERAGE_FORMS.info("");
        }
    }

    private void logDetails(Logger logger, String[] details, int max) {
        Stream.of(details).limit(max).forEach(
                xp -> logger.info("\t" + xp)
        );
        if (details.length>max) {
            logger.info("\t .. and " + (details.length-max) + " more");
        }
    }

    private void logDetails(Logger logger, Collection details, int max) {
        details.stream().limit(max).forEach(
                xp -> logger.info("\t" + xp)
        );
        if (details.size()>max) {
            logger.info("\t .. and " + (details.size()-max) + " more");
        }
    }

    @Override
    public String toString() {
        return "LoggingFuzzerListener{" +
                "logProgressInterval=" + logProgressInterval +
                '}';
    }
}
