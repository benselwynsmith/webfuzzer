package nz.ac.vuw.httpfuzz.jee;


import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.ExecutionPoint;
import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.http.RequestSpec;

import java.io.File;
import java.util.Set;

/**
 * Abstract class to facilitate implementation of the FuzzerListener interface.
 * @author jens dietrich
 */
public class AbstractFuzzerListener implements FuzzerListener  {

    protected void checkLogFolder(File folder) {
        Preconditions.checkState(folder!=null,"Log folder must not be null");
        Preconditions.checkState(!(folder.exists() && !folder.isDirectory()),"Log folder is not a folder: " + folder);
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    @Override
    public void fuzzingStarts() {}

    @Override
    public void requestRejected(RequestSpec request) {}

    @Override
    public void requestSent(RequestSpec request, int statusCode) {}

    @Override
    public void feedbackRequestSent(String ticketId, int statusCode) {}

    @Override
    public void newExecutionPointsDiscovered(RequestSpec requestSpec, Set<ExecutionPoint> executionPoints) {}

    @Override
    public void newRequestParameterNamesDiscovered(RequestSpec requestSpec, Set<String> requestParameterNames) {}

    @Override
    public void newInvocationsOfCriticalMethodDiscovered(RequestSpec requestSpec, String stackTrace) {}

    @Override
    public void newXSSDiscovered(RequestSpec requestSpec, String response, String token) {}

    @Override
    public void newFormsDiscovered(RequestSpec requestSpec, Set<Form> forms) {}
}
