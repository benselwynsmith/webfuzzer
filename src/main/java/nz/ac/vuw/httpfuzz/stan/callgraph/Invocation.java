package nz.ac.vuw.httpfuzz.stan.callgraph;

import nz.ac.vuw.httpfuzz.commons.graph.Edge;

/**
 * Representation of invocations.
 * @author jens  dietrich
 */
public class Invocation extends Edge<Method> {

    private String callsite = null;
    public Invocation(Method start, Method end, String callsite) {
        super(start, end);
        this.callsite = callsite;
    }

    public String getCallsite() {
        return callsite;
    }
}
