package nz.ac.vuw.httpfuzz.stan.pointsto;

/**
 * Representation of null. In doop, this corresponds to "null pseudo heap".
 * Singleton.
 * @author jens dietrich
 */
public class Null extends HObject{

    // must be initialised depending on the language used
    public static String TYPE_NAME = null;

    private Null() {}

    public static Null DEFAULT = new Null();

    public String getType() {
        assert TYPE_NAME != null;
        return TYPE_NAME;
    }

}
