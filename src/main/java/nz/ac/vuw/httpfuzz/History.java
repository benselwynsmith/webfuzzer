package nz.ac.vuw.httpfuzz;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import nz.ac.vuw.httpfuzz.http.RequestSpec;

import java.util.Map;

/**
 * Default history implementation based on  a  guava cache with max size.
 * @author jens dietrich
 */
public interface History {

    /**
     * Return true if this spec hasn't been seen yet.
     * This can be implemented using a probabilistic data structure.
     * @param spec
     * @return
     */
    boolean isNew(RequestSpec spec);

    /**
     * Add a spec.
     * @param spec
     */
    void keep(RequestSpec spec);

    /**
     * Get the count of duplicates that have been rejected.
     * @return
     */
    long getDuplicatesDetectedCount();

}
