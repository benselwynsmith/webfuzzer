package nz.ac.vuw.httpfuzz.commons;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Utility to redirect output of OS processes to log system.
 * @author jens dietrich
 */
public final class StreamGobbler extends Thread {
    private InputStream is;
    private String name;
    private String task;
    private Logger logger;

    // reads everything from is until empty.
    public StreamGobbler(String name, InputStream is, String task, Logger logger) {
        this.is = is;
        this.name = name;
        this.task = task;
        this.logger = logger;
    }

    public void run() {
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ((line = br.readLine()) != null) {
                logger.debug(line);
            }
            logger.debug("Finished process output redirecting to log: " + name);
        } catch (IOException ioe) {
            logger.error(ioe);
        }
    }
}
