package nz.ac.vuw.httpfuzz.commons;

import org.apache.log4j.Logger;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Utility to unpack a war.
 * Note that the classes in the classes folder of the war are re-packaged into a jar
 * to facilitate processing by tools that require jars as input.
 * @author jens dietrich
 */
public class WarUnpacker {

    private static Logger LOGGER = LogSystem.getLogger(WarUnpacker.class);
    public static final String CLASSES_JAR = "__classes.jar";

    public static List<File> unpackAndAddSystemLibs(File war)  {
        List<File> jars = new ArrayList<>();
        jars.addAll(unpack(war));
        jars.addAll(Environment.DEFAULT.getJREJars());
        jars.addAll(Environment.DEFAULT.getJEEJars());
        return jars;
    }

    public static List<File> unpack(File war)  {
        File tmp = IOUtils.DEFAULT.createTmpDir("__"+ WarUnpacker.class.getName() + "__"+ war.getName());
        File tmpClasses = new File(tmp,"classes");
        tmpClasses.mkdirs();

        List<File> jars = new ArrayList<>();
        int jarCount = 0;
        int classCount = 0;
        try (ZipFile zip = new ZipFile(war)) {
            for (Enumeration en = zip.entries();en.hasMoreElements();) {
                ZipEntry next = (ZipEntry)en.nextElement();
                if (next.getName().startsWith("WEB-INF/lib") && next.getName().endsWith(".jar")) {
                    File jar = new File(tmp,next.getName().substring(next.getName().lastIndexOf('/')));
                    LOGGER.info("Extracting jar: " + jar);
                    Files.copy(zip.getInputStream(next), Paths.get(jar.getAbsolutePath()));
                    jars.add(jar);
                    jarCount = jarCount + 1;
                }
                else if (next.getName().startsWith("WEB-INF/classes") && next.getName().endsWith(".class")) {
                    File klass = new File(tmp,next.getName());
                    klass.getParentFile().mkdirs();
                    LOGGER.info("Extracting class: " + klass);
                    Files.copy(zip.getInputStream(next), Paths.get(klass.getAbsolutePath()));
                    classCount = classCount + 1;
                }
            }
        }
        catch (IOException x) {
            LOGGER.error("Error analysing archive (not a zip archive?)",x);
        }

        LOGGER.info("Jars extracted from war: " + jarCount);
        LOGGER.info("Classes extracted from war: " + classCount);


        // creating classes jar
        File classesJar = new File(tmp,CLASSES_JAR);
        LOGGER.info("Creating jar for classes extracted from war");
        List<String> command = new ArrayList<>();
        command.add("jar");
        command.add("-cvf");
        command.add(classesJar.getAbsolutePath());
        command.add("-C");
        command.add(new File(tmp,"WEB-INF/classes").getAbsolutePath());
        command.add(".");

        try {
            Logger LOGGER2 = LogSystem.getLogger(WarUnpacker.class.getSimpleName() + "::jar");
            ProcessBuilder builder = new ProcessBuilder(command);
            Process process = builder.start();
            StreamGobbler errorGobbler = new StreamGobbler("err",process.getErrorStream(),"jar",LOGGER2);
            StreamGobbler outputGobbler = new StreamGobbler("out",process.getInputStream(),"jar",LOGGER2);
            errorGobbler.start();
            outputGobbler.start();
            process.waitFor();
            LOGGER.info("Built jar for classes extracted from war: " + classesJar.getAbsolutePath());
        }
        catch (Exception x) {
            LOGGER.error("Error jaring classes extracted from war",x);
        }

        jars.add(0,classesJar);

        return jars;
    }
}
