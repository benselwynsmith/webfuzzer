package nz.ac.vuw.httpfuzz;

/**
 * Constants for the runtime component. Copy of nz.ac.vuw.httpfuzz.jee.rt.Constants .
 * TODO: replace by dependency, by using different project structire with mvn aggregator.
 * @author jens dietrich
 */
public interface RTConstants {


        // response header used to send ticket ids back to fuzzer, this can then be used to pick up
        // details gathered via instrumentation in a separate request
        String FUZZING_FEEDBACK_TICKET_HEADER = "WEBFUZZ-FEEDBACK-TICKET";

        // URL path for requests to pick up information using ticket
        // URLs to be used: <FUZZING_FEEDBACK_PATH_TOKEN>/<ticket>
        String FUZZING_FEEDBACK_PATH_TOKEN = "/__fuzzing_feedback";

        // request header used to mark up tainted input in the request
        // values are comma separated
        String TAINTED_INPUT_HEADER = "WEBFUZZ-TAINTED-VALUES";

}
