package nz.ac.vuw.httpfuzz.html;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import nz.ac.vuw.httpfuzz.Profile;
import nz.ac.vuw.httpfuzz.http.MethodSpec;
import nz.ac.vuw.httpfuzz.jee.Loggers;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * HTML parser utility, based on jsoup.
 * @author jens dietrich
 */
public class HtmlParser {

    // cache parsed form -- common use case: extract forms and links from the same form
    private static final CacheLoader<String, Document> cacheLoader = new CacheLoader<String, Document>() {
        @Override
        public Document load(String html) {
            try {
                return Jsoup.parse(html);
            }
            catch (Exception x) {
                Loggers.FUZZER.warn("Error parsing html");
            }
            return null;
        }
    };
    private static final LoadingCache<String,Document> cache = CacheBuilder
            .newBuilder()
            .softValues()
            .maximumSize(10_000)
            .expireAfterAccess(10,TimeUnit.MINUTES)
            .build(cacheLoader);

    public static Collection<Form> extractForms(Profile profile, String responseString,String owner) throws Exception {
        Document document = cache.get(responseString);
        return extractForms(profile,document,owner);
    }

    public static Collection<Form> extractForms(Profile profile, InputStream in,String owner) throws Exception {
        Document document = Jsoup.parse(in, Charset.defaultCharset().name(),profile.getServerAndPortURL());
        return extractForms(profile,document,owner);
    }

    public static Collection<Form> extractForms(Profile profile,Document document,String owner) {
        List<Form> forms = new ArrayList<>();
        Elements htmlForms = document.select("form");
        for (Element htmlForm : htmlForms) {
            Form form = new Form();
            form.setOwner(owner);
            String method = htmlForm.attributes().get("method");
            if (method != null) {
                method = method.toUpperCase();
                if (method.equals("GET")) {
                    form.setMethod(MethodSpec.GET);
                }
                else if (method.equals("POST")) {
                    form.setMethod(MethodSpec.POST);
                }
            }

            String action = htmlForm.attributes().get("action");
            action = toUrl(profile,action);
            if (action!=null) {
                form.setAction(action);

                Elements elements = htmlForm.select("input");
                for (Element element : elements) {
                    Input input = new Input();
                    input.setName(element.attributes().get("name"));
                    input.setType(element.attributes().get("type"));
                    input.setValue(element.attributes().get("value"));
                    form.addInput(input);
                }
                forms.add(form);
            }
        }
        return forms;
    }

    public static Collection<URL> extractLinks(Profile profile, String responseString) throws Exception {
        List<URL> links = new ArrayList<>();
        Document document = cache.get(responseString);
        Elements htmlLinks = document.select("a");
        for (Element htmlLink : htmlLinks) {
            String href = htmlLink.attributes().get("href");
            if (href!=null) {
                href = toUrl(profile,href);
                links.add(new URL(href)); // TODO: prepend host + port + context
            }
        }

        // TODO extract internal iframes

        return links;
    }

    // TODO investigate how relative urls will be resolved, e.g. of relPath starts with / or ~
    private static String toUrl(Profile profile, String relPath) {

        Loggers.FUZZER.debug("converting form action to URL: " + relPath);

        String url = null;

        if (relPath.startsWith("http:") || relPath.startsWith("https:")) {
            // establish whether this is "internal" or "external" , skip if external as this is out of scope for fuzzing
            if (relPath.startsWith(profile.getServerURL())) {
                url = relPath;
            }
            else {
                // check for localhost
                if (profile.getHost().equals("localhost")) {
                    if (profile.getProtocol() == Profile.Protocol.http && relPath.startsWith("http://127.0.0.1")) {
                        url =  relPath;
                    }
                    else if (profile.getProtocol() == Profile.Protocol.https && relPath.startsWith("https://127.0.0.1")) {
                        url =  relPath;
                    }
                }
                url =  null;
            }
        }
        else if (relPath.startsWith("~")) {
            Loggers.FUZZER.warn("translation of these form actions not yet implemented: " + relPath);
            url =  toUrl(profile,relPath.substring(1));
        }
        else if (relPath.startsWith("/")) {
            url =  profile.getServerAndPortURL() + relPath ;
        }
        else {
            url =  profile.getWebappRootURL() + "/" + relPath ;
        }
        return url;

    }
}
