package nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

/**
 * Crossover operation.
 * @author jens dietrich
 */
@FunctionalInterface
public interface Crossover<T extends Spec>  {
    T apply(SourceOfRandomness sourceOfRandomness, Context context, T parent1, T parent2);
}
