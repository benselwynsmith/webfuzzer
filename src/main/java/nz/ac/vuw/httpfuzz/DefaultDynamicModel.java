package nz.ac.vuw.httpfuzz;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.commons.LogSystem;
import nz.ac.vuw.httpfuzz.feedback.CoverageGraph;
import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import java.net.HttpCookie;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Default dynamic model.
 * @author jens dietrich
 */
public class DefaultDynamicModel implements DynamicModel {

    private static Logger LOGGER = LogSystem.getLogger("dyn-model");

    private enum ResponseType {OK,CLIENT_ERROR,SERVER_ERROR};
    private CoverageGraph coverageGraph = new CoverageGraph();
    private Map<RequestSpec,Object> targets = new ConcurrentHashMap<>();
    private Set<String> requestParameterNames = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private Set<Form> forms = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private Set<String> stacktraces = Collections.newSetFromMap(new ConcurrentHashMap<>());

    @Override
    public Set<ExecutionPoint> executionsRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, Collection<ExecutionPoint> executions) {

        ResponseType responseType = getResponseType(response);
        Set<ExecutionPoint> newExecutionPoints = coverageGraph.add(requestSpec, executions);
        if (newExecutionPoints.size()>0) {
            LOGGER.info("Register " + requestSpec + " triggering " + newExecutionPoints.size() + " new executions");
        }
        if (responseType==ResponseType.SERVER_ERROR) {
            targets.put(requestSpec,response.getStatusLine()); // TODO: add more info
            //LOGGER.info("Target reached by " + requestSpec + " -- status line is: " + response.getStatusLine());
        }
        return newExecutionPoints == null ? Collections.emptySet() : newExecutionPoints ;
    }

    @Override
    public Set<String> requestParametersRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, Collection<String> requestParameterNames) {
        Set<String> newRequestParameterNames = new HashSet<>();
        for (String requestParameterName:requestParameterNames) {
            if (this.requestParameterNames.add(requestParameterName)) {
                newRequestParameterNames.add(requestParameterName);
            }
        }
        return newRequestParameterNames;
    }

    @Override
    public Set<Form> formsRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, Collection<Form> forms) {
        Set<Form> newForms = new HashSet<>();
        for (Form form:forms) {
            if (this.forms.add(form)) {
                newForms.add(form);
            }
        }
        return newForms;
    }

    @Override
    public boolean stacktraceToCriticalMethodRecorded(RequestSpec requestSpec, HttpRequest request, HttpResponse response, String stacktrace) {
        return this.stacktraces.add(stacktrace);
    }

    @Override
    public Collection<RequestSpec> getTopRequestSpecs() {
        return coverageGraph.getTopVertices();
    }

    @Override
    public Collection<RequestSpec> getTopRequestSpecs(int depth) {
        return coverageGraph.getTopVertices(depth);
    }

    @Override
    public Collection<Form> getForms() {
        return forms;
    }

    @Override
    public Collection<RequestSpec> getRequestSpecs() {
        return coverageGraph.getRequestSpecs();
    }

    @Override
    public Map<RequestSpec,Object> getTargets() {
        return this.targets;
    }

    @Override
    public Set<String> getRequestParameterNames() {
        return requestParameterNames;
    }

    private static ResponseType getResponseType(HttpResponse response) {
        int responseCode = response.getStatusLine().getStatusCode();
        Preconditions.checkState(responseCode>=0,"Invalid status code: " + responseCode);
        Preconditions.checkState(responseCode<600,"Invalid status code: " + responseCode);

        if (responseCode<400) {
            return ResponseType.OK;
        }
        else if (responseCode<500) {
            return ResponseType.CLIENT_ERROR;
        }
        else if (responseCode<600) {
            return ResponseType.SERVER_ERROR;
        }
        else {
            assert false; // should be unreachable
            return null;
        }
    }


}
