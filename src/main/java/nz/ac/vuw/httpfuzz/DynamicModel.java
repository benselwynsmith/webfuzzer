package nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import java.net.HttpCookie;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Dynamic model.
 * @author jens dietrich
 */
public interface DynamicModel {
    /**
     * Record executions (in moost cases, methods) that have been encountered.
     * @param requestSpec
     * @param request
     * @param response
     * @param executions
     * @return newly added (as in: not seen before) execution points
     */
    Set<ExecutionPoint> executionsRecorded (
            RequestSpec requestSpec,
            HttpRequest request,
            HttpResponse response,
            Collection<ExecutionPoint> executions
    );

    /**
     * Record request parameters that have been encountered.
     * @param requestSpec
     * @param request
     * @param response
     * @param requestParameters
     * @return newly added (as in: not seen before) request parameter names
     */
    Set<String> requestParametersRecorded (
            RequestSpec requestSpec,
            HttpRequest request,
            HttpResponse response,
            Collection<String> requestParameters
    );


    /**
     * Record a form returned by the server.
     * @param requestSpec
     * @param request
     * @param response
     * @param forms
     * @return newly found forms
     */
    Set<Form> formsRecorded(
            RequestSpec requestSpec,
            HttpRequest request,
            HttpResponse response,
            Collection<Form> forms
    );

    /**
     * Record a new stack trace for an invocation of a critical method.
     * @param requestSpec
     * @param request
     * @param response
     * @param stacktrace
     * @return
     */
    boolean stacktraceToCriticalMethodRecorded (
        RequestSpec requestSpec,
        HttpRequest request,
        HttpResponse response,
        String stacktrace
    );



    // NOTE: disabled, now all connections use a shared cookiestore
    //    /**
    //     * Get the cookies sent by the server so far.
    //     * @return a collection of cookies
    //     */
    //    Collection<HttpCookie> getCookies();

    /**
     * Get the top-performing request specs.
     * Can be used to mutate them.
     */
    Collection<RequestSpec> getTopRequestSpecs();

    /**
     * Get forms recorded.
     * @return
     */
    Collection<Form> getForms();

    /**
     * Get the top-performing request specs.
     * Can be used to mutate them.
     * @param depth the depth -- 0 means top, 1 means top or second best etc
     */
    Collection<RequestSpec> getTopRequestSpecs(int depth);

    /**
     * Get all request specs.
     * @author jens dietrich
     */
    Collection<RequestSpec> getRequestSpecs();

    /**
     * Get the targets reached.
     * The info values are to be specified, this should contain status lines, methods invoked and further similar information,
     */

    Map<RequestSpec, Object> getTargets();


    /**
     * Get request parameter names encountered.
     * @return
     */
    Set<String> getRequestParameterNames();


}
