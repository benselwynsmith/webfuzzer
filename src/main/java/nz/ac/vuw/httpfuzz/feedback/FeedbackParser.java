package nz.ac.vuw.httpfuzz.feedback;

import nz.ac.vuw.httpfuzz.ExecutionPoint;
import nz.ac.vuw.httpfuzz.jee.war.MethodInvocation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.*;

/**
 * Simple feedback parser.
 * @author jens dietrich
 */
public class FeedbackParser {

    public static Set<ExecutionPoint> extractMethodSpecs(JSONObject json) {
        try {
            JSONArray arr = json.getJSONArray("invokedMethods");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
            Set<ExecutionPoint> specs = new HashSet<>();
            for (int i = 0; i < arr.length(); i++) {
                String line = arr.getString(i);
                String[] parts = line.split("::");
                assert parts.length == 3;
                ExecutionPoint spec = new MethodInvocation(parts[0], parts[1], parts[2]);
                specs.add(spec);
            }
            return specs;
        }
        catch (JSONException x) {
            return Collections.EMPTY_SET;
        }
    }

    public static Set<String> extractRequestParameterNames(JSONObject json) {
        return convertFlatJSONArray(json, "requestParameterNames");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
    }

    public static Set<String> extractStackTracesForUnsafeMethods(JSONObject json) {
        return convertFlatJSONArray(json,"unsafeSinkInvocationStackTraces");  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
    }

    private static Set<String> convertFlatJSONArray(JSONObject json,String key)  {
        try {
            JSONArray arr = json.getJSONArray(key);  // constant in webfuzzer=rt nz.ac.vuw.httpfuzz.jee.rt.DataKind
            Set<String> values = new HashSet<>();
            for (int i = 0; i < arr.length(); i++) {
                values.add(""+arr.get(i));
            }
            return values;
        }
        catch (JSONException x) {
            return Collections.EMPTY_SET;
        }
    }
}
