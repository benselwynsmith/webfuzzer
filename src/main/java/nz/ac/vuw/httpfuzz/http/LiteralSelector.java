package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.StaticModel;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.util.EnumSet;
import java.util.function.Supplier;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Utility to select literals.
 * @author jens dietrich
 */
public class LiteralSelector {

    enum Kind {STRING,INT,NAME};

    // note that numerical values will be converted to strings
    public static String choseLiteral(SourceOfRandomness sor,Context context) {
        StaticModel model = context.getStaticModel();
        Object literal = null;
        int counter = 0;
        final int THRESHOLD = 100;

        // note that some choices might not be valid, i.e. there might be no literals available
        // in this case we try again
        // this has the potenial for infinite loop !
        // after a threshold is reached, a random string will be returned
        while (literal==null) {
            counter= counter+1;
            if (counter==THRESHOLD) {
                literal = sor.defaultRandomString();
                break;
            }
            Supplier<Kind> kindGenerator = ProbabilisticSupplierFactory.create()
                    .withProbability(60).createValue(() -> Kind.STRING)
                    .withProbability(20).createValue(() -> Kind.INT)
                    .withProbability(20).createValue(() -> Kind.NAME)
                    .build();
            Kind kind = kindGenerator.get();

            Supplier<StaticModel.Scope> scopeGenerator = ProbabilisticSupplierFactory.create()
                    .withProbability(70).createValue(() -> StaticModel.Scope.APPLICATION)
                    .withProbability(30).createValue(() -> StaticModel.Scope.LIBRARIES)
                    .build();
            StaticModel.Scope scope = scopeGenerator.get();

            switch (kind) {
                case STRING: {
                    literal = sor.choose(model.getStringLiterals(EnumSet.of(scope)));
                }
                case INT: {
                    literal = sor.choose(model.getIntLiterals(EnumSet.of(scope)));
                }
                case NAME: {
                    literal = sor.choose(model.getReflectiveNames(EnumSet.of(scope)));
                }
            }
        }

        if (literal!=null) {
            if (!(literal instanceof String)) {
                literal = ""+literal;
            }
            return (String)literal;
        }
        assert false;
        return null;
    }
}
