package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Mutator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.EnumSet;
import java.util.function.Supplier;

/**
 * Modifies an existing parameter Spec.
 * @author jens dietrich
 */
public class ParameterSpecMutator implements Mutator<ParameterSpec> {

    private ParameterKeyGenerator keyGenerator = new ParameterKeyGenerator();
    private ParameterValueGenerator valueGenerator = new ParameterValueGenerator();

    @Override
    public ParameterSpec apply(SourceOfRandomness sourceOfRandomness, Context context, ParameterSpec parameterSpec) {
        Supplier<ParameterSpec> specs = ProbabilisticSupplierFactory.create()
            .withProbability(80)
            .createValue(
                () -> {
                    // use same key, but new value
                    return new ParameterSpec(
                        parameterSpec.getKey(), valueGenerator.apply(sourceOfRandomness,context), EnumSet.of(SpecSource.MUTATED)
                    );
                })
            .withProbability(20)
            .createValue(
                () -> {
                    // use same value, but new key
                    return new ParameterSpec(
                        keyGenerator.apply(sourceOfRandomness,context),parameterSpec.getValue(),EnumSet.of(SpecSource.MUTATED)
                    );
                }
            )
            .build();
        return specs.get();
    }

}
