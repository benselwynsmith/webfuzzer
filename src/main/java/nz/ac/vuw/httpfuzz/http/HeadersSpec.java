package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Spec;
import nz.ac.vuw.httpfuzz.SpecSource;

import java.util.*;

/**
 * Representation of (multiple) headers.
 * Headers are managed as sets, assuming the order is irrelevant.
 * @author jens dietrich
 */
public class HeadersSpec implements Spec,VisitableSpec {

    private List<HeaderSpec> headers = new ArrayList<>();
    // to control duplicate keys producing invalid URIs
    private Set<String> keys = new HashSet<>();

    private EnumSet<SpecSource> sources = EnumSet.noneOf(SpecSource.class);

    public List<HeaderSpec> getHeaders() {
        return headers;
    }

    @Override
    public EnumSet<SpecSource> getSources() {
        return sources;
    }

    public void add(HeaderSpec headerSpec) {
        if (this.keys.add(headerSpec.getKey().getValue())) {
            // only one source, dont merge
            this.headers.add(headerSpec);
            this.sources.addAll(headerSpec.getSources());
        }
    }

    // use case: add RANDOM if number is random
    public void add(SpecSource specSource) {
        this.sources.add(specSource);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HeadersSpec that = (HeadersSpec) o;
        return headers.equals(that.headers);
    }

    @Override
    public int hashCode() {
        return headers.hashCode();
    }

    @Override
    public String toString() {
        return "HeadersSpec{" +
                "headers=" + headers +
                '}';
    }

    @Override
    public void accept(SpecVisitor visitor) {
        if (visitor.visit(this)) {
            for (HeaderSpec spec:headers) {
                spec.accept(visitor);
            }
        }
        visitor.endVisit(this);
    }
}
