package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Spec;
import nz.ac.vuw.httpfuzz.SpecSource;
import org.apache.http.client.utils.URIBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Representation of an URISpec.
 * @author jens dietrich
 */
public class URISpec implements Spec,VisitableSpec {
    private String scheme = "http";
    private String host = null;
    private int port = 80;
    private List<ValueSpec<String>> pathSpec = new ArrayList<>();
    private List<String> path = new ArrayList<>();
    private boolean wildCardPath = false;
    private EnumSet<SpecSource> sources = null;

    public URISpec() {
        this.sources = sources;
    }

    @Override
    public EnumSet<SpecSource> getSources() {
        return sources;
    }

    void setProvenance(EnumSet<SpecSource> sources) {
        this.sources = sources;
    }

    public void setWildCardPath(boolean wildCardPath) {
        this.wildCardPath = wildCardPath;
    }
    public boolean isWildCardPath() {
        return wildCardPath;
    }
    public String getScheme() {
        return scheme;
    }

    public List<ValueSpec<String>> getPathSpec() {
        return pathSpec;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public List<String> getPath() {
        return path;
    }

    public void addToPath(ValueSpec<String> part) {
        String value = part.getValue();
        if (!value.equals(""))
            this.pathSpec.add(part);
            this.path.add(value);
    }

    public void addAllToPath(List<ValueSpec<String>> parts) {
        for (ValueSpec<String> part:parts) {
            this.addToPath(part);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        URISpec uriSpec = (URISpec) o;

        if (port != uriSpec.port) return false;
        if (!scheme.equals(uriSpec.scheme)) return false;
        if (!host.equals(uriSpec.host)) return false;
        return path.equals(uriSpec.path);
    }

    @Override
    public int hashCode() {
        int result = scheme.hashCode();
        result = 31 * result + host.hashCode();
        result = 31 * result + port;
        result = 31 * result + path.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String s = "URISpec{" + scheme + "://" + host + ':' + port;
        if (this.path!=null) {
            String p = path.stream().collect(Collectors.joining("/"));
            if (p!=null && p.length()>0) {
                s = s + '/' + p;
            }
        }
        return  s + '}';
    }

    public URI toURI() throws URISyntaxException {
        String p = path.stream().collect(Collectors.joining("/"));
        return new URIBuilder()
            .setScheme(this.scheme)
            .setHost(this.host)
            .setPort(this.port)
            .setPath(p)
            .build();
    }

    @Override
    public void accept(SpecVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }
}
