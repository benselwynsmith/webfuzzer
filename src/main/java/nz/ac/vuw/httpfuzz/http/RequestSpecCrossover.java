package nz.ac.vuw.httpfuzz.http;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;

/**
 * Produces request specs based on mutating hi-scoring previous
 * @author jens dietrich
 */
public class RequestSpecCrossover implements Generator<RequestSpec> {

    private ParametersSpecCrossover parametersCrossover = new ParametersSpecCrossover();

    public RequestSpecCrossover() {
        super();
    }

    @Override
    public RequestSpec apply(SourceOfRandomness sor, Context context) {

        Collection<RequestSpec> highScoringSpecs = context.getDynamicModel().getTopRequestSpecs();
        Preconditions.checkState(highScoringSpecs.size()>0);
        Collection<RequestSpec> specs = context.getDynamicModel().getRequestSpecs();
        Preconditions.checkState(specs.size()>0);

        // pick one -- 50%  chance to pick high-scoring, 50% any
        RequestSpec parent1 = sor.nextBoolean() ? sor.choose(highScoringSpecs) : sor.choose(specs);
        RequestSpec parent2 = sor.nextBoolean() ? sor.choose(highScoringSpecs) : sor.choose(specs);


        if (parent1==parent2) {
            return parent1;
        }

        RequestSpec requestSpec = new RequestSpec();
        requestSpec.addParent(parent1);
        requestSpec.addParent(parent2);
        EnumSet<SpecSource> provenance = parent1.getSources().clone();
        provenance.addAll(parent2.getSources());
        provenance.add(SpecSource.CROSSOVER);
        requestSpec.setProvenance(provenance);

        requestSpec.setParametersSpec(this.parametersCrossover.apply(sor,context,parent1.getParametersSpec(),parent2.getParametersSpec()));

        // TODO: crossbreed those as well
        requestSpec.setUriSpec(sor.nextBoolean() ? parent1.getUriSpec() : parent2.getUriSpec());
        requestSpec.setHeadersSpec(sor.nextBoolean() ? parent1.getHeadersSpec() : parent2.getHeadersSpec());
        requestSpec.setMethodSpec(sor.nextBoolean() ? parent1.getMethodSpec() : parent2.getMethodSpec());

        requestSpec.setGenerator(RequestSpec.Generator.CROSSOVER);

        return requestSpec;
    }
}
