package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Mutator;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Modifies an existing parameter Spec.
 * @author jens dietrich
 */
public class ParametersSpecMutator implements Mutator<ParametersSpec> {

    // all in %
    public static final int PROBABILITY_TO_MUTATE_EACH = 20;
    public static final int PROBABILITY_TO_ADD_ONE = 20;
    public static final int PROBABILITY_TO_REMOVE_EACH = 10;

    private ParameterSpecMutator paramMutator = new ParameterSpecMutator();
    private ParameterSpecGenerator paramGenerator = new ParameterSpecGenerator();

    @Override
    public ParametersSpec apply(SourceOfRandomness sourceOfRandomness, Context context, ParametersSpec parametersSpec) {

        ParametersSpec mutatedParamSpec = new ParametersSpec();

        for (ParameterSpec paramSpec:parametersSpec.getParams()) {
            if (sourceOfRandomness.trueAt(PROBABILITY_TO_REMOVE_EACH)) {
                // remove, i.e., dont add
            }
            else if (sourceOfRandomness.trueAt(PROBABILITY_TO_MUTATE_EACH)) {
                mutatedParamSpec.add(paramMutator.apply(sourceOfRandomness,context,paramSpec));
            }
        }
        if (sourceOfRandomness.trueAt(PROBABILITY_TO_ADD_ONE)) {
            mutatedParamSpec.add(paramGenerator.apply(sourceOfRandomness,context));
        }

        return mutatedParamSpec;
    }

}
