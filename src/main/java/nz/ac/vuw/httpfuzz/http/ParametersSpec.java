package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Spec;
import nz.ac.vuw.httpfuzz.SpecSource;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * Representation of (multiple) request parameters.
 * Parameters are managed as sets, assuming the order is irrelevant.
 * @author jens dietrich
 */
public class ParametersSpec implements Spec,VisitableSpec {

    private Set<ParameterSpec> params = new HashSet<>();
    private EnumSet<SpecSource> sources = EnumSet.noneOf(SpecSource.class);

    @Override
    public EnumSet<SpecSource> getSources() {
        return sources;
    }

    // to control duplicate keys producing invalid URIs
    private Set<String> keys = new HashSet<>();

    public Set<ParameterSpec> getParams() {
        return params;
    }

    public void add(ParameterSpec parameterSpec) {
        if (this.keys.add(parameterSpec.getKey().getValue())) {
            // only one source, dont merge
            this.params.add(parameterSpec);
            this.sources.addAll(parameterSpec.getSources());
        }
    }

    // use case: add RANDOM if number is random
    public void add(SpecSource specSource) {
        this.sources.add(specSource);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParametersSpec that = (ParametersSpec) o;
        return params.equals(that.params);
    }

    @Override
    public int hashCode() {
        return params.hashCode();
    }

    @Override
    public String toString() {
        return "ParametersSpec{" +
                "params=" + params +
                '}';
    }

    @Override
    public void accept(SpecVisitor visitor) {
        if (visitor.visit(this)) {
            for (ParameterSpec spec:params) {
                spec.accept(visitor);
            }
        }
        visitor.endVisit(this);
    }
}
