package nz.ac.vuw.httpfuzz.http;

import com.google.common.collect.Sets;
import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Crossover;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.Set;

/**
 * Representation of a request parameter.
 * @author jens dietrich
 */
public class ParametersSpecCrossover implements Crossover<ParametersSpec> {

    public final int PROBABILITY_TO_ADD_UNCOMMON = 50; // in %

    @Override
    public ParametersSpec apply(SourceOfRandomness sourceOfRandomness, Context context, ParametersSpec parent1, ParametersSpec parent2) {

        ParametersSpec offspring = new ParametersSpec();
        Set<ParameterSpec> commons = Sets.intersection(parent1.getParams(),parent2.getParams());

        for (ParameterSpec common:commons) {
            offspring.add(common);
        }

        // add params they dont have in common with some probability
        for (ParameterSpec param:parent1.getParams()) {
            if (!commons.contains(param) && sourceOfRandomness.trueAt(PROBABILITY_TO_ADD_UNCOMMON)) {
                offspring.add(param);
            }
        }
        for (ParameterSpec param:parent2.getParams()) {
            if (!commons.contains(param) && sourceOfRandomness.trueAt(PROBABILITY_TO_ADD_UNCOMMON)) {
                offspring.add(param);
            }
        }

        return offspring;
    }
}
