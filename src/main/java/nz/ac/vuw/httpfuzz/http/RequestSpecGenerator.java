package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.function.Supplier;

/**
 * Produces request specs.
 * @author jens dietrich
 */
public class RequestSpecGenerator implements Generator<RequestSpec> {

    private RandomRequestSpecGenerator randomGenerator = new RandomRequestSpecGenerator();
    private RequestSpecMutator mutatingGenerator = new RequestSpecMutator();
    private RequestSpecCrossover crossoverGenerator = new RequestSpecCrossover();
    private RequestFromCapturedFormGenerator fromFormGenerator = new RequestFromCapturedFormGenerator();

    public RequestSpecGenerator() {
        super();
    }

    @Override
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        try {
            int highScoringRequestCount = context.getDynamicModel().getTopRequestSpecs().size();

            int[] split = null; // probabilities in percent -- {GENERATE,MUTATE,CROSSBREED,FORM}

            int formCount = context.getDynamicModel().getForms().size() + context.getStaticModel().getForms().size();

            if (highScoringRequestCount == 0) {
                split = formCount==0 ? new int[]{100, 0, 0, 0} : new int[]{90, 0, 0, 10};
            } else if (highScoringRequestCount < 5) {
                split = formCount==0  ? new int[]{80, 20, 0, 0} : new int[]{70, 15, 0, 15};
            } else {
                split = formCount==0  ? new int[]{60, 20, 20, 0} : new int[]{45, 20, 20, 15};
            }

            assert split.length == 4;
            assert split[0] + split[1] + split[2] + split[3] == 100;

            try {
                Supplier<RequestSpec> requestSpecs;
                ProbabilisticSupplierFactory supplierFactory = ProbabilisticSupplierFactory.create()
                        .withProbability(split[0]).createValue(() -> randomGenerator.apply(sourceOfRandomness, context))
                        .withProbability(split[1]).createValue(() -> mutatingGenerator.apply(sourceOfRandomness, context))
                        .withProbability(split[2]).createValue(() -> crossoverGenerator.apply(sourceOfRandomness, context));
                if (formCount>00 )
                    requestSpecs = supplierFactory.withProbability(split[3]).createValue(() -> fromFormGenerator.apply(sourceOfRandomness, context)).build();
                else
                    requestSpecs = supplierFactory.build();

                RequestSpec requestSpec = requestSpecs.get();
                
                return requestSpec;
            } catch (Exception x) {
                x.printStackTrace();
            }
            return null;
        }
        catch (Exception x) {
            x.printStackTrace();
            return null;
        }

    }


}
