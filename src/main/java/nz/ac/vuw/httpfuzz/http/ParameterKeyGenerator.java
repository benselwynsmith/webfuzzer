package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.StaticModel;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Generator for parameter keys.
 * @author jens dietrich
 */
public class ParameterKeyGenerator implements Generator<ValueSpec<String>> {

    @Override
    public ValueSpec<String> apply(SourceOfRandomness sor, Context context) {
        Set<String> dynamicRequestKeys = context.getDynamicModel().getRequestParameterNames();
        Set<String> staticRequestKeys = context.getStaticModel().getRequestParameterNames();

        Set<String> literals = context.getStaticModel().getStringLiterals(EnumSet.of(StaticModel.Scope.APPLICATION, StaticModel.Scope.LIBRARIES));
        Supplier<ValueSpec<String>> supplier = null;

        if (staticRequestKeys.size()>0 && dynamicRequestKeys.size()>0) {
            supplier = ProbabilisticSupplierFactory.create()
                .withProbability(40).createValue(() -> new ValueSpec<String>(sor.choose(dynamicRequestKeys),EnumSet.of(SpecSource.DYNAMIC_ANALYSIS)))
                .withProbability(15).createValue(() -> new ValueSpec<String>(sor.choose(staticRequestKeys),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(15).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
                .withProbability(15).createValue(() -> new ValueSpec<String>(sor.choose(literals),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(15).createValue(() -> new ValueSpec<String>(randomAlphanumeric(sor.getRandomPositiveNumberFromParetoDistribution(100)),EnumSet.of(SpecSource.RANDOM))) // random string
                .build();
        }
        else if (staticRequestKeys.size()==0 && dynamicRequestKeys.size()>0) {
            supplier = ProbabilisticSupplierFactory.create()
                .withProbability(55).createValue(() -> new ValueSpec<>(sor.choose(dynamicRequestKeys),EnumSet.of(SpecSource.DYNAMIC_ANALYSIS)))
                .withProbability(15).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
                .withProbability(15).createValue(() -> new ValueSpec<String>(sor.choose(literals),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(15).createValue(() -> new ValueSpec<String>(randomAlphanumeric(sor.getRandomPositiveNumberFromParetoDistribution(100)),EnumSet.of(SpecSource.RANDOM))) // random string
                .build();
        }
        else if (staticRequestKeys.size()>0 && dynamicRequestKeys.size()==0) {
            supplier = ProbabilisticSupplierFactory.create()
                .withProbability(55).createValue(() -> new ValueSpec<String>(sor.choose(staticRequestKeys),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(15).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
                .withProbability(15).createValue(() -> new ValueSpec<String>(sor.choose(literals),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(15).createValue(() -> new ValueSpec<String>(randomAlphanumeric(sor.getRandomPositiveNumberFromParetoDistribution(100)),EnumSet.of(SpecSource.RANDOM))) // random string
                .build();
        }
        else {
            supplier = ProbabilisticSupplierFactory.create()
                .withProbability(40).createValue(() -> new ValueSpec<String>(randomAlphanumeric(sor.getRandomPositiveNumberFromParetoDistribution(100)),EnumSet.of(SpecSource.RANDOM)))
                .withProbability(40).createValue(() -> new ValueSpec<String>(sor.choose(literals),EnumSet.of(SpecSource.STATIC_ANALYSIS)))
                .withProbability(20).createValue(() -> TaintedStringValueGenerator.DEFAULT.apply(sor,context))
                .build();
        }
        assert supplier!=null;
        ValueSpec<String> value = supplier.get();

        // handle null -- in case one of the lists is empty !
        return value==null ? new ValueSpec<>(sor.defaultRandomString(),EnumSet.of(SpecSource.STATIC_ANALYSIS)): value;

    }
}
