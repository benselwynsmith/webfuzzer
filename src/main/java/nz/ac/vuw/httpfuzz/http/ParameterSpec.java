package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Spec;
import nz.ac.vuw.httpfuzz.SpecSource;

import java.util.EnumSet;

/**
 * Representation of a request parameter.
 * @author jens dietrich
 */
public class ParameterSpec implements Spec,VisitableSpec {

    private ValueSpec<String> key = null;
    private ValueSpec<String> value = null;
    private EnumSet<SpecSource> sources = null;

    public ParameterSpec(ValueSpec<String> key, ValueSpec<String> value, EnumSet<SpecSource> sources) {
        this.key = key;
        this.value = value;
        this.sources = sources;
    }

    public ValueSpec<String> getKey() {
        return key;
    }

    public ValueSpec<String> getValue() {
        return value;
    }

    @Override
    public EnumSet<SpecSource> getSources() {
        return sources;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParameterSpec that = (ParameterSpec) o;

        if (key != null ? !key.equals(that.key) : that.key != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ParameterSpec{" + key.getValue() + '=' + value.getValue() + '}';
    }

    @Override
    public void accept(SpecVisitor visitor) {
        if (visitor.visit(this)) {
            this.key.accept(visitor);
            this.value.accept(visitor);
        }
        visitor.endVisit(this);
    }
}
