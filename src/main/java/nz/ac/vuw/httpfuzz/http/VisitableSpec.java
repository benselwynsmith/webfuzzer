package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Spec;

/**
 * Visitable spec.
 * @author jens dietrich
 */
public interface VisitableSpec {

    void accept(SpecVisitor visitor);
}
