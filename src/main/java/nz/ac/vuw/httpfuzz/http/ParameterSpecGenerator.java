package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.EnumSet;

/**
 * Produces key-value pair to be used as http parameters.
 * Note that they are not encoded, this is done when the actual requests are built.
 * @author jens dietrich
 */
public class ParameterSpecGenerator implements Generator<ParameterSpec> {

    private ParameterKeyGenerator keyGenerator = new ParameterKeyGenerator();
    private ParameterValueGenerator valueGenerator = new ParameterValueGenerator();

    @Override
    public ParameterSpec apply(SourceOfRandomness sor, Context context) {
        ValueSpec<String> key = this.keyGenerator.apply(sor,context);
        ValueSpec<String> value = this.valueGenerator.apply(sor,context);
        return new ParameterSpec(key, value, SpecProvenanceUtils.merge(key.getSources(),value.getSources(), EnumSet.of(SpecSource.RANDOM)));
    }
}
