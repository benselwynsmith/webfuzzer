package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.StaticModel;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.RandomUtils;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import nz.ac.vuw.httpfuzz.html.Form;
import nz.ac.vuw.httpfuzz.html.Input;
import nz.ac.vuw.httpfuzz.jee.Loggers;
import org.apache.commons.lang3.RandomStringUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.function.Supplier;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Generate a request from a form.
 * @author jens dietrich
 */
public abstract class AbstractRequestFromCapturedFormGenerator implements Generator<RequestSpec> {

    // this is used to point to the origin of the form
    protected abstract EnumSet<SpecSource> getProvenance();

    protected abstract Form getForm(SourceOfRandomness sourceOfRandomness, Context context);
    protected ParameterValueGenerator defaultParameterValueGenerator = new ParameterValueGenerator();

    @Override
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {

        Form form = getForm(sourceOfRandomness, context);

        // method
        MethodSpec methodSpec = null;
        if (form.getMethod()!=null) {
            methodSpec = MethodSpec.valueOf(form.getMethod().name());
        }
        else {
            Set<MethodSpec> supportedMethods = context.getProfile().getUsedMethods();
            Set<MethodSpec> candidateMethods = new HashSet<>();
            if (supportedMethods.contains(MethodSpec.GET)) candidateMethods.add(MethodSpec.GET);
            if (supportedMethods.contains(MethodSpec.POST)) candidateMethods.add(MethodSpec.POST);
            assert candidateMethods.size()>0;
            methodSpec = sourceOfRandomness.choose(candidateMethods);
        }

        // uri
        URISpec uriSpec = null;
        if (form.getAction()!=null) {
            uriSpec = new URISpec();
            try {
                Loggers.FUZZER.debug("About to mutate form with action: " + form.getAction() + " " + " source: " + this.getProvenance());
                URL url = new URL(form.getAction());
                uriSpec.setScheme(url.getProtocol());
                uriSpec.setHost(url.getHost());
                uriSpec.setPort(url.getPort()==-1?context.getProfile().getPort():url.getPort());
                String[] path = url.getPath().split("/");
                for (String part:path) {
                    uriSpec.addToPath(new ValueSpec<>(part,getProvenance()));
                }
                EnumSet<SpecSource> provenance = getProvenance();
                uriSpec.setProvenance(provenance);
            } catch (MalformedURLException e) {
                Loggers.FUZZER.warn("Cannot parse URL to mutate form " + form.getAction());
            }
        }
        else {
            uriSpec = new URISpecGenerator().apply(sourceOfRandomness,context);
        }

        // headers
        HeadersSpec headersSpec = new HeadersSpec();


//                new HeadersSpecGenerator().apply(sourceOfRandomness,context);
//
//        // start aggregating
        RequestSpec requestSpec = new RequestSpec();
        requestSpec.setMethodSpec(methodSpec);
        requestSpec.setUriSpec(uriSpec);
        requestSpec.setHeadersSpec(headersSpec);
        requestSpec.setGenerator(RequestSpec.Generator.FORM);
        if (form.getOwner()!=null) {
            requestSpec.addProperty(RequestSpec.FORM_OWNER, form.getOwner());
        }

        // parameters
        ParametersSpec parametersSpec = new ParametersSpec();
        List<Input> inputs = new ArrayList<>(form.getInputs().size());
        inputs.addAll(form.getInputs());
        Collections.shuffle(inputs); // in case there are multiple inputs for the same name
        for (Input input:inputs) {
            String name = input.getName();
            if (name!=null) {
                ValueSpec valueSpec = createParameterValue(input,sourceOfRandomness,context);
                if (valueSpec!=null) {
                    parametersSpec.add(new ParameterSpec(new ValueSpec<String>(name, EnumSet.of(SpecSource.DYNAMIC_ANALYSIS)), valueSpec, EnumSet.of(SpecSource.DYNAMIC_ANALYSIS)));
                }
            }
        }
        requestSpec.setParametersSpec(parametersSpec);

        requestSpec.setProvenance(EnumSet.of(SpecSource.DYNAMIC_ANALYSIS));

        return requestSpec;
    }

    // generation depending on type, see https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/checkbox
    // TODO this is incomplete
    protected ValueSpec createParameterValue(Input input, SourceOfRandomness sourceOfRandomness, Context context) {
        String defaultValue = input.getValue();
        String type = "text";

        if (input.getType()!=null) {
            type = input.getType().toLowerCase().trim();
        }

        if (type.equals("hidden")) {
            return defaultValue==null ? null : new ValueSpec(defaultValue,getProvenance());
        }
        else if (type.equals("checkbox")) {
            // randomise check / uncheck
            if (sourceOfRandomness.nextBoolean()) {
                // checked
                return defaultValue==null ? new ValueSpec("on",getProvenance()) : new ValueSpec(defaultValue,getProvenance());
            }
            else {
                // unchecked
                return null;
            }
        }
        else if (type.equals("radio")) {
            if (defaultValue==null) {
                return null; // shoud not happen
            }
            // randomise check / uncheck
            return sourceOfRandomness.nextBoolean() ? new ValueSpec(defaultValue,getProvenance()) : null ;
        }
        else if (type.equals("submit") || type.equals("button")) {
            return null;
        }
        else if (type.equals("number")) {

            // use default if possible
            if (defaultValue!=null && !defaultValue.equals("")) {
                return new ValueSpec(defaultValue,getProvenance());
            }

            Supplier<ValueSpec<String>> supplier = ProbabilisticSupplierFactory.create()
                .withProbability(60).createValue(() -> new ValueSpec<>("" + sourceOfRandomness.getRandomPositiveNumberFromParetoDistribution(0.25, Integer.MAX_VALUE),EnumSet.of(SpecSource.RANDOM)))
                .withProbability(20).createValue(() -> new ValueSpec<>("" + -sourceOfRandomness.getRandomPositiveNumberFromParetoDistribution(0.25, Integer.MAX_VALUE),EnumSet.of(SpecSource.RANDOM)))
                .withProbability(20).createValue(() -> new ValueSpec<>("" +sourceOfRandomness.nextDouble(-100,1000),EnumSet.of(SpecSource.RANDOM)))
                .build() ;

            return supplier.get();

        }
        else if (type.equals("text") || type.equals("password") || type.trim().equals("")) {
            // default behaviour , but do not log
            // TODO: this is overfitted for testing
            new ValueSpec<>(RandomStringUtils.randomAlphanumeric(10),EnumSet.of(SpecSource.RANDOM));
        }
        else {
            Loggers.FUZZER.warn("Targeted form value generation to supported for input type " + type);
        }
        // default bahaviour
        return defaultParameterValueGenerator.apply(sourceOfRandomness,context);
    }


}
