package nz.ac.vuw.httpfuzz.http;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import nz.ac.vuw.httpfuzz.SpecSource;
import nz.ac.vuw.httpfuzz.jee.AbstractFuzzer;

import java.util.EnumSet;

/**
 * Utilities to deal with spec provenance.
 * @author jens dietrich
 */
public class SpecProvenanceUtils {
    public static EnumSet<SpecSource> merge(EnumSet<SpecSource>... elements) {
        EnumSet<SpecSource> merged = EnumSet.noneOf(SpecSource.class);
        for (EnumSet<SpecSource> element:elements) {
            merged.addAll(element);
        }
        return merged;
    }

    public static Multimap<String, RequestSpec.RequestDataKind> extractTaintedParts(RequestSpec requestSpec) {
        return extractParts(requestSpec,SpecSource.TAINTED);
    }

    public static Multimap<String, RequestSpec.RequestDataKind> extractParts(RequestSpec requestSpec,SpecSource source) {
        Multimap<String, RequestSpec.RequestDataKind> requestData = HashMultimap.create();
        SpecVisitor visitor = new SpecVisitor() {
            @Override
            public boolean visit(HeaderSpec spec) {
                String key = spec.getKey().getValue();
                if (spec.getKey().getSources().contains(source)) {
                    if (!HeaderSpecGenerator.getStandardHeaderNames().contains(key)) {
                        requestData.put(key, RequestSpec.RequestDataKind.HEADER_KEY);
                    }
                }
                if (spec.getValue().getSources().contains(source)) {
                    requestData.put(spec.getValue().getValue(), RequestSpec.RequestDataKind.HEADER_VALUE);
                }
                return true;
            }

            @Override
            public boolean visit(ParameterSpec spec) {
                if (spec.getKey().getSources().contains(source)) {
                    requestData.put(spec.getKey().getValue(), RequestSpec.RequestDataKind.PARAMETER_KEY);
                }
                if (spec.getValue().getSources().contains(source)) {
                    requestData.put(spec.getValue().getValue(), RequestSpec.RequestDataKind.PARAMETER_VALUE);
                }
                return true;
            }

            @Override
            public boolean visit(URISpec spec) {
                for (ValueSpec<String> pathElement : spec.getPathSpec()) {
                    // ignore the parts of the path that are always there
                    if (pathElement.getSources().contains(source)) {
                        requestData.put(pathElement.getValue(), RequestSpec.RequestDataKind.PATH);
                    }
                }
                return true;
            }
        };
        requestSpec.accept(visitor);
        return requestData;
    }
}
