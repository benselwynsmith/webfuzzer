package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.*;
import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;

import java.util.EnumSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

/**
 * Mutator for URIs.
 * @author shawn
 */
public class URISpecMutator implements Mutator<URISpec> {

    @Override
    public URISpec apply(SourceOfRandomness sor, Context context, URISpec originalUri) {

        URISpec uri = new URISpec();

        EnumSet<SpecSource> provenance = originalUri.getSources().clone();
        provenance.add(SpecSource.MUTATED);
        uri.setProvenance(provenance);
        uri.setScheme(originalUri.getScheme());
        uri.setHost(originalUri.getHost());
        uri.setPort(originalUri.getPort());
        uri.setWildCardPath(originalUri.isWildCardPath());
        uri.addAllToPath(originalUri.getPathSpec());
        if (originalUri.getPath().size() < 10 && originalUri.isWildCardPath()) { // TODO path length blows up, which shouldnt happen as it should grow with new coverage
            uri.addToPath(URISpecGenerator.randomURLString(sor,context));
        }
        return uri;
    }

}
