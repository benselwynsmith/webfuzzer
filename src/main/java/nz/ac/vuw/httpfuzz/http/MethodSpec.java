package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Spec;
import nz.ac.vuw.httpfuzz.SpecSource;

import java.util.EnumSet;

/**
 * Representation of http methods as defined in https://tools.ietf.org/html/rfc7230/
 * @author jens dietrich
 */
public enum MethodSpec implements Spec,VisitableSpec {

    GET,HEAD,POST,PUT,DELETE,TRACE,OPTIONS,CONNECT,PATCH;

    public static final EnumSet<MethodSpec> WEB_APP_METHODS = EnumSet.of(GET,POST);

    public static final EnumSet<MethodSpec> REST_METHODS = EnumSet.of(GET,POST,PATCH,DELETE);

    public static final EnumSet<MethodSpec> ALL = EnumSet.allOf(MethodSpec.class);

    // add random as one of those will be randomly picked
    // not completely kosher, TODO: add RANDOM in MethodSpecGenerator
    public static final EnumSet<SpecSource> SOURCES = EnumSet.of(SpecSource.FIXED,SpecSource.RANDOM);

    @Override
    public void accept(SpecVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

    @Override
    public EnumSet<SpecSource> getSources() {
        return SOURCES;
    }
}
