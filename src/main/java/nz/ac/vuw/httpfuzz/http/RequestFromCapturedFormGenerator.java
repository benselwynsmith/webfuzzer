package nz.ac.vuw.httpfuzz.http;

import nz.ac.vuw.httpfuzz.Context;
import nz.ac.vuw.httpfuzz.Generator;
import nz.ac.vuw.httpfuzz.commons.SourceOfRandomness;
import nz.ac.vuw.httpfuzz.html.Form;

import java.util.Collection;

/**
 * GHenerates requests from forms captured in the static pre-analysis, or from feedback.
 * @author jens dietrich
 */
public class RequestFromCapturedFormGenerator implements Generator<RequestSpec>  {

    private RequestFromDynamicallyCapturedFormGenerator generateFromDynamicallyCapturedForms = new RequestFromDynamicallyCapturedFormGenerator();
    private RequestFromStaticallyCapturedFormGenerator generateFromStaticallyCapturedForms = new RequestFromStaticallyCapturedFormGenerator();

    @Override
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        Collection<Form> sforms = context.getStaticModel().getForms();
        Collection<Form> dforms = context.getDynamicModel().getForms();
        boolean selection = sourceOfRandomness.trueBasedOnSizeRatio(sforms,dforms);
        return selection ? generateFromStaticallyCapturedForms.apply(sourceOfRandomness,context) : generateFromDynamicallyCapturedForms.apply(sourceOfRandomness,context);
    }
}
