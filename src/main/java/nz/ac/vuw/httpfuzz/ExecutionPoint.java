package nz.ac.vuw.httpfuzz;

/**
 * Abstract representation of some code being executed during fuzzing.
 * Example: method invocations, used by coverage-based fuzzing.
 * @author jens dietrich
 */
public interface ExecutionPoint {
}
