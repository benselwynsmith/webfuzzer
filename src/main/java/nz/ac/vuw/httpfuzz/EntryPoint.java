package nz.ac.vuw.httpfuzz;

import nz.ac.vuw.httpfuzz.http.MethodSpec;

import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

/**
 * Specification of an entry point into a web application.
 * An entry has a URL pattern that can be used for fuzzing, and links this with Java classes implementing this, these
 * associations can be used to create additional static pre-analyses.
 * @author jens dietrich
 */
public class EntryPoint {

    private String urlPattern = null;
    private Set<MethodSpec> supportedMethods = EnumSet.allOf(MethodSpec.class);
    private String className = null;

    public EntryPoint(String urlPattern, String className, Set<MethodSpec> supportedMethods) {
        this.urlPattern = urlPattern;
        this.className = className;
        this.supportedMethods = supportedMethods;
    }

    public EntryPoint(String urlPattern,String className) {
        this.urlPattern = urlPattern;
        this.className = className;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public Set<MethodSpec> getSupportedMethods() {
        return supportedMethods;
    }

    public String getClassName() {
        return className;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntryPoint that = (EntryPoint) o;
        return Objects.equals(urlPattern, that.urlPattern) &&
                Objects.equals(supportedMethods, that.supportedMethods) &&
                Objects.equals(className, that.className);
    }

    @Override
    public int hashCode() {
        return Objects.hash(urlPattern, supportedMethods, className);
    }

    @Override
    public String toString() {
        return "EntryPoint{" +
                "urlPattern='" + urlPattern + '\'' +
                ", supportedMethods=" + supportedMethods +
                ", className='" + className + '\'' +
                '}';
    }
}
