package test.nz.ac.vuw.httpfuzz.jee.war;

import nz.ac.vuw.httpfuzz.jee.war.ReflectiveNamesExtractor;
import nz.ac.vuw.httpfuzz.jee.war.SimpleWarScopeModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Set;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertSame;

public class ReflectiveNamesExtractorTest {

    private ReflectiveNamesExtractor extractor = null;

    @Before
    public void setup() {
        extractor = new ReflectiveNamesExtractor(new SimpleWarScopeModel());
    }

    @After
    public void after() {
        extractor = null;
    }

    private InputStream cl() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(BeanClass.class.getName().replace('.','/')+".class").getFile());
        System.out.println(file.getAbsolutePath());
        System.out.println(file.exists());
        return new FileInputStream(file);
    }

    @Test
    public void testProperties() throws  Exception {
        InputStream in = cl();
        Set<String> names = this.extractor.extractFromClassFile(cl());
        assertSame(9, names.size());
        assertTrue(names.contains("int"));
        assertTrue(names.contains("boolean"));
        in.close();
    }


}
