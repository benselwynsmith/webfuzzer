package test.nz.ac.vuw.httpfuzz.feedback;

import nz.ac.vuw.httpfuzz.feedback.CoverageGraph;
import nz.ac.vuw.httpfuzz.http.RequestSpec;
import nz.ac.vuw.httpfuzz.http.URISpec;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestCoverageGraph {

    private Map<String,MockExecutionPoint> cache = null;

    @Before
    public void setup() {
        cache = new HashMap<>();
    }

    @After
    public void teardown() {
        cache = null;
    }

    private Collection<MockExecutionPoint> x(String... names) {
        return Stream.of(names)
                .map(n -> cache.computeIfAbsent(n, n2 -> new MockExecutionPoint(n2)))
                .collect(Collectors.toSet());
    }

    @Test
    public void test1() {
        CoverageGraph coverageGraph = new CoverageGraph();

        RequestSpec req1 = spec("req1");
        RequestSpec req2 = spec("req2");
        RequestSpec req3 = spec("req3");
        RequestSpec req4 = spec("req4");
        RequestSpec req5 = spec("req5");

        coverageGraph.add(req1,x("a()","b()"));
        coverageGraph.add(req2,x("a()","c()"));
        coverageGraph.add(req3,x("a()","c()","d()"));
        coverageGraph.add(req4,x("a()","c()","e()"));
        coverageGraph.add(req5,x("f()"));
        // coverageGraph.debugGraph();
        Collection<RequestSpec> topScoringKeys = coverageGraph.getTopVertices();

        assertEquals(4,topScoringKeys.size());
        assertTrue(topScoringKeys.contains(req1));
        assertTrue(topScoringKeys.contains(req3));
        assertTrue(topScoringKeys.contains(req4));
        assertTrue(topScoringKeys.contains(req5));
    }

    @Test
    public void test2() {
        CoverageGraph coverageGraph = new CoverageGraph();

        RequestSpec req1 = spec("req1");
        RequestSpec req2 = spec("req2");
        RequestSpec req3 = spec("req3");
        RequestSpec req4 = spec("req4");
        RequestSpec req5 = spec("req5");

        coverageGraph.add(req1,x("a()","b()"));
        coverageGraph.add(req2,x("a()","c()"));
        coverageGraph.add(req3,x("a()","c()","d()"));
        coverageGraph.add(req4,x("a()","c()","e()"));
        coverageGraph.add(req5,x("f()"));
        // coverageGraph.debugGraph();
        Collection<RequestSpec> topScoringKeys = coverageGraph.getTopVertices(0);

        assertEquals(4,topScoringKeys.size());
        assertTrue(topScoringKeys.contains(req1));
        assertTrue(topScoringKeys.contains(req3));
        assertTrue(topScoringKeys.contains(req4));
        assertTrue(topScoringKeys.contains(req5));
    }

    @Test
    public void test3() {
        CoverageGraph coverageGraph = new CoverageGraph();

        RequestSpec req1 = spec("req1");
        RequestSpec req2 = spec("req2");
        RequestSpec req3 = spec("req3");
        RequestSpec req4 = spec("req4");
        RequestSpec req5 = spec("req5");

        coverageGraph.add(req1,x("a()","b()"));
        coverageGraph.add(req2,x("a()","c()"));
        coverageGraph.add(req3,x("a()","c()","d()"));
        coverageGraph.add(req4,x("a()","c()","e()"));
        coverageGraph.add(req5,x("f()"));
        // coverageGraph.debugGraph();
        Collection<RequestSpec> topScoringKeys = coverageGraph.getTopVertices(1);

        assertEquals(5,topScoringKeys.size());
        assertTrue(topScoringKeys.contains(req1));
        assertTrue(topScoringKeys.contains(req2));
        assertTrue(topScoringKeys.contains(req3));
        assertTrue(topScoringKeys.contains(req4));
        assertTrue(topScoringKeys.contains(req5));
    }

    @Test
    public void test4() {
        CoverageGraph coverageGraph = new CoverageGraph();

        RequestSpec req1 = spec("req1");
        RequestSpec req2 = spec("req2");
        RequestSpec req3 = spec("req3");
        RequestSpec req4 = spec("req4");
        RequestSpec req5 = spec("req5");

        coverageGraph.add(req1,x("a()","b()"));
        coverageGraph.add(req2,x("a()","c()"));
        coverageGraph.add(req3,x("a()","c()","d()"));
        coverageGraph.add(req4,x("a()","c()","e()"));
        coverageGraph.add(req5,x("f()"));
        // coverageGraph.debugGraph();
        Collection<RequestSpec> topScoringKeys = coverageGraph.getTopVertices(1);

        assertEquals(5,topScoringKeys.size());
        assertTrue(topScoringKeys.contains(req1));
        assertTrue(topScoringKeys.contains(req2));
        assertTrue(topScoringKeys.contains(req3));
        assertTrue(topScoringKeys.contains(req4));
        assertTrue(topScoringKeys.contains(req5));
    }

    @Test
    public void test5() {
        CoverageGraph coverageGraph = new CoverageGraph();

        RequestSpec req1 = spec("req1");
        RequestSpec req2 = spec("req2");
        RequestSpec req3 = spec("req3");
        RequestSpec req4 = spec("req4");
        RequestSpec req5 = spec("req5");

        coverageGraph.add(req1,x("a()"));
        coverageGraph.add(req2,x("a()","b()"));
        coverageGraph.add(req3,x("a()","b()","c()"));
        coverageGraph.add(req4,x("a()","b()","c()","d()"));
        coverageGraph.add(req5,x("a()","b()","c()","d()","e()"));
        // coverageGraph.debugGraph();
        Collection<RequestSpec> topScoringKeys = coverageGraph.getTopVertices(1);

        assertEquals(1,coverageGraph.getTopVertices(0).size());
        assertEquals(2,coverageGraph.getTopVertices(1).size());
        assertEquals(3,coverageGraph.getTopVertices(2).size());
        assertEquals(4,coverageGraph.getTopVertices(3).size());
        assertEquals(5,coverageGraph.getTopVertices(4).size());
        assertEquals(5,coverageGraph.getTopVertices(5).size());
    }


    @Test
    public void test6() {
        CoverageGraph coverageGraph = new CoverageGraph();

        RequestSpec req1 = spec("req1");
        RequestSpec req2 = spec("req2");
        RequestSpec req3 = spec("req3");
        RequestSpec req4 = spec("req4");

        coverageGraph.add(req1,x("a()"));
        coverageGraph.add(req2,x("a()","b()"));
        coverageGraph.add(req3,x("a()","c()"));
        coverageGraph.add(req4,x("a()","b()","c()","d()"));// diamond structure
        // coverageGraph.debugGraph();
        Collection<RequestSpec> topScoringKeys = coverageGraph.getTopVertices();

        assertEquals(1,topScoringKeys.size());
        assertTrue(topScoringKeys.contains(req4));
    }

    private RequestSpec spec(String n) {
    RequestSpec spec = new RequestSpec();
    URISpec uriSpec = new URISpec();
    uriSpec.setHost(n);
    spec.setUriSpec(uriSpec);
    return spec;
    }
}
