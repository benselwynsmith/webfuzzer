package test.nz.ac.vuw.httpfuzz.jee.war;

import com.google.common.base.Preconditions;
import nz.ac.vuw.httpfuzz.EntryPoint;
import nz.ac.vuw.httpfuzz.Profile;
import nz.ac.vuw.httpfuzz.StaticModel;
import nz.ac.vuw.httpfuzz.http.MethodSpec;
import nz.ac.vuw.httpfuzz.jee.war.EntryPointExtractor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.EnumSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class EntryPointExtractionFromWebXMLTest {
    private File war = null;
    private static EnumSet<StaticModel.Scope> SCOPES = EnumSet.of(StaticModel.Scope.APPLICATION);

    @Before
    public void setup() throws Exception {
        war = new File("src/test/resources/war-webxml1/target/war-webxml1-1.0.0.war");
        Preconditions.checkState(war.exists(),"war " + war.getAbsolutePath() + " does not exist, check whether test project has been built with \"mvn package\"");
    }

    @After
    public void tearDown () {
        war = null;
    }

    @Test
    public void test () throws Exception {
        Set<EntryPoint> entryPoints = EntryPointExtractor.extractEntryPoints(war,new Profile.WebApplicationProfile("localhost",80));

        assertEquals(1,entryPoints.size());
        EntryPoint entryPoint = entryPoints.iterator().next();

        assertEquals("main",entryPoint.getUrlPattern());
        assertEquals("example1.MainServlet",entryPoint.getClassName());
        assertEquals(1,entryPoint.getSupportedMethods().size());
        assertEquals(MethodSpec.GET,entryPoint.getSupportedMethods().iterator().next());

    }

}
