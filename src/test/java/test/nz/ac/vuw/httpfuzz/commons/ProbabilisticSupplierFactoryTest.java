package test.nz.ac.vuw.httpfuzz.commons;

import nz.ac.vuw.httpfuzz.commons.ProbabilisticSupplierFactory;
import org.junit.Test;
import java.util.Random;
import java.util.function.Supplier;
import static org.junit.Assert.assertEquals;

public class ProbabilisticSupplierFactoryTest {

    // note that this test in inherently flaky
    @Test
    public void testDistribution1() {
        Supplier<String> supplier = ProbabilisticSupplierFactory.create()
            .withProbability(10).createValue(() -> "A")
            .withProbability(90).createValue(() -> "B")
            .build() ;

        int MAX = 1_000_000;
        int countA = 0;
        int countB = 0;
        for (int i=0;i<MAX;i++) {
            String value = supplier.get();
            if (value.equals("A")) {
                countA = countA + 1;
            }
            else if (value.equals("B")) {
                countB = countB + 1;
            }
        }

        System.out.println("As : " + countA);
        System.out.println("Bs : " + countB);

        assertEquals(MAX,countA + countB);

        double ratioA = ((double)countA) / ((double) (countA+countB));
        System.out.println("A ratio : " + ratioA);
        assertEquals(0.1d,ratioA,0.01);
    }

    @Test
    public void testDistribution2() {

        // simulate condition with extewrnal random generator
        Random random = new Random();

        Supplier<String> supplier = ProbabilisticSupplierFactory.create()
                .onCondition(() -> random.nextInt(3)==0).createValue(() -> "C")
                .withProbability(10).createValue(() -> "A")
                .withProbability(90).createValue(() -> "B")
                .build() ;

        int MAX = 1_000_000;
        int countA = 0;
        int countB = 0;
        int countC = 0;
        for (int i=0;i<MAX;i++) {
            String value = supplier.get();
            if (value.equals("A")) {
                countA = countA + 1;
            }
            else if (value.equals("B")) {
                countB = countB + 1;
            }
            else if (value.equals("C")) {
                countC = countC + 1;
            }
        }

        System.out.println("As : " + countA);
        System.out.println("Bs : " + countB);
        System.out.println("Cs : " + countC);

        assertEquals(MAX,countA + countB + countC);

        double ratioA = ((double)countA) / ((double) (countA+countB));
        double ratioC = ((double)countC) / ((double) (countA+countB+countC));
        System.out.println("A ratio : " + ratioA);
        System.out.println("C ratio : " + ratioC);

        assertEquals(0.1d,ratioA,0.01);
        assertEquals(0.33d,ratioC,0.01);
    }


    @Test(expected=IllegalStateException.class)
    public void testIntegrity1 () {
        ProbabilisticSupplierFactory.create()
            .withProbability(20)
            .createValue(() -> "A")
            .withProbability(90)
            .createValue(() -> "B")
            .build() ;
    }

    @Test(expected=IllegalStateException.class)
    public void testIntegrity2 () {
        ProbabilisticSupplierFactory.create()
            .withProbability(20)
            .createValue(() -> "A")
            .withProbability(70)
            .createValue(() -> "B")
            .build() ;
    }

    @Test(expected=IllegalArgumentException.class)
    public void testIntegrity3 () {
        ProbabilisticSupplierFactory.create()
            .withProbability(20)
            .createValue(() -> "A")
            .withProbability(-1)
            .createValue(() -> "B")
            .build() ;
    }

    @Test(expected=IllegalStateException.class)
    public void testIntegrity4 () {
        ProbabilisticSupplierFactory.create()
            .onCondition(() -> true)
            .withProbability(100)
            .createValue(() -> "B")
            .build() ;
    }



}
