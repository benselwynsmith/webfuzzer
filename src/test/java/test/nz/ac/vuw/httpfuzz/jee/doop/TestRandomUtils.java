package test.nz.ac.vuw.httpfuzz.jee.doop;

import com.google.common.math.Stats;
import nz.ac.vuw.httpfuzz.commons.RandomUtils;
import org.junit.Test;
import java.util.*;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class TestRandomUtils {
    @Test
    public void testExponentialRandomNumberDistribution() throws Exception {

        final double mean = 5;
        final int max = 42;
        Map<Integer,Integer> stats = new TreeMap<>();
        for (int i=0;i<100_000;i++) {
            int value = RandomUtils.getRandomPositiveNumberFromExponentialDistribution(mean,max);
            stats.compute(value,(k,v) -> v==null?1:v+1);
        }

        for (Integer k:stats.keySet()) {
            System.out.println(k + " -> " + stats.get(k));
        }

        List<Integer> values = new ArrayList<>();
        for (Integer k:stats.keySet())  {
            for (int i=0;i<stats.get(k);i++) {
                values.add(k);
            }
        }

        Stats gstats = Stats.of(values);
        assertEquals(mean,gstats.mean(),0.1);
        assertTrue(gstats.max()<=(double)max);
        assertTrue(gstats.min()>=(double)0);

    }

    @Test
    public void testParetoRandomNumberDistribution() throws Exception {

        final double mean = 5;
        final int max = 42;
        Map<Integer,Integer> stats = new TreeMap<>();
        for (int i=0;i<100_000;i++) {
            int value = RandomUtils.getRandomPositiveNumberFromParetoDistribution(42);
            stats.compute(value,(k,v) -> v==null?1:v+1);
        }

        for (Integer k:stats.keySet()) {
            System.out.println(k + " -> " + stats.get(k));
        }

        List<Integer> values = new ArrayList<>();
        for (Integer k:stats.keySet())  {
            for (int i=0;i<stats.get(k);i++) {
                values.add(k);
            }
        }

        Stats gstats = Stats.of(values);
        assertTrue(gstats.mean()<5);
        assertTrue(gstats.max()<=max);

    }
}
