package test.nz.ac.vuw.httpfuzz.jee.war;

public class ClassWithConstants {


    private String stringField = "sliteral3";
    private int smallIntField = 2;
    private short shortField = (short)1000;
    private int largeIntField = 42_000_000;

    public String useStringLiteral1(String s) {
        return s + "sliteral1" + this.stringField;
    }

    public String useStringLiteral2() {
        return useStringLiteral1("sliteral2");
    }


    public int useSmallIntLiteral1(int i) {
        return i + 3 + this.smallIntField;
    }

    public int useSmallIntLiteral2() {
        return useSmallIntLiteral1(42);
    }

    public int useLargeIntLiteral1(int i) {
        return i + 1_000_000 + this.largeIntField;
    }

    public int useLargeIntLiteral2() {
        return useSmallIntLiteral1(2_000_000);
    }

    public short useShortLiteral1(short i) {
        short j = (short)10000;
        return (short)(i + j + this.shortField);
    }

    public int useShortLiteral2() {
        return useShortLiteral1((short)20000);
    }
}


