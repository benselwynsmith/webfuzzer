package test.nz.ac.vuw.httpfuzz.jee.war;

import nz.ac.vuw.httpfuzz.jee.war.LiteralExtractor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Set;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class LiteralExtractorTest {

    private LiteralExtractor extractor = null;

    @Before
    public void setup() {
        extractor = new LiteralExtractor();
    }

    @After
    public void after() {
        extractor = null;
    }

    private InputStream cl() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(ClassWithConstants.class.getName().replace('.','/')+".class").getFile());
        System.out.println(file.getAbsolutePath());
        System.out.println(file.exists());
        return new FileInputStream(file);
    }

    @Test
    public void testStringLiterals() throws  Exception {
        InputStream in = cl();
        Set<String> literals = this.extractor.extractStringLiterals(cl());
        assertSame(3,literals.size());
        assertTrue(literals.contains("sliteral1"));
        assertTrue(literals.contains("sliteral2"));
        assertTrue(literals.contains("sliteral3"));
        in.close();
    }

    @Test
    public void testIntLiterals() throws  Exception {
        InputStream in = cl();
        Set<Integer> literals = this.extractor.extractIntLiterals(cl());
        assertSame(9,literals.size());
        in.close();
    }

    @Test
    public void testSmallIntLiterals() throws  Exception {
        InputStream in = cl();
        Set<Integer> literals = this.extractor.extractIntLiterals(cl());
        assertTrue(literals.contains(3));
        assertTrue(literals.contains(42));
        assertTrue(literals.contains(2));
        in.close();
    }

    @Test
    public void testLargeIntLiterals() throws  Exception {
        InputStream in = cl();
        Set<Integer> literals = this.extractor.extractIntLiterals(cl());
        assertTrue(literals.contains(1_000_000));
        assertTrue(literals.contains(2_000_000));
        assertTrue(literals.contains(42_000_000));
        in.close();
    }

    @Test
    public void testShortIntLiterals() throws  Exception {
        InputStream in = cl();
        Set<Integer> literals = this.extractor.extractIntLiterals(cl());
        assertTrue(literals.contains(10_000));
        assertTrue(literals.contains(20_000));
        assertTrue(literals.contains(1_000));
        in.close();
    }
}
