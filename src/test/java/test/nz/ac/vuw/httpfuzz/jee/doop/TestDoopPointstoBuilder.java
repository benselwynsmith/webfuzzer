package test.nz.ac.vuw.httpfuzz.jee.doop;

import nz.ac.vuw.httpfuzz.jee.doop.PointstoBuilder;
import nz.ac.vuw.httpfuzz.stan.pointsto.*;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collection;
import java.util.Set;
import static org.junit.Assert.*;

public class TestDoopPointstoBuilder {

    private static PointstoBuilder.Config pointstoConfig = new PointstoBuilder.Config();

    @BeforeClass
    public static void setupPointstoConfig() {
        pointstoConfig.tolerateErrors = false;
        pointstoConfig.lineFilter = l -> true;
        pointstoConfig.variableFilter = v -> true;
        pointstoConfig.objectFilter = o -> true;
    }

    @Test
    public void testObject1() throws Exception {
        String doopOutput =
            "<<immutable-hcontext>>\t<java.lang.System: void initializeSystemClass()>/new java.io.BufferedInputStream/0\t<<immutable-context>>\t<java.lang.System: void setIn0(java.io.InputStream)>/@parameter0";
        Reader in = new StringReader(doopOutput);
        Pointsto pointsto = new PointstoBuilder().build(in,pointstoConfig);

        Set<Variable> variables = pointsto.getVariables();
        assertEquals(1,variables.size());

        // <java.lang.System: void setIn0(java.io.InputStream)>/@parameter0
        Variable var = variables.iterator().next();
        assertEquals("java.lang.System",var.getMethod().getClassName());
        assertEquals("setIn0",var.getMethod().getMethodName());
        assertEquals("(java.io.InputStream)void",var.getMethod().getDescriptor());
        assertEquals("@parameter0",var.getName());

        Collection<HObject> objects = pointsto.getObjects(var);
        assertEquals(1,objects.size());

        // <java.lang.System: void initializeSystemClass()>/new java.io.BufferedInputStream/0
        HObject obj = objects.iterator().next();
        assertTrue(obj instanceof Allocation);
        Allocation alloc = (Allocation)obj;
        assertEquals("java.lang.System",alloc.getMethod().getClassName());
        assertEquals("initializeSystemClass",alloc.getMethod().getMethodName());
        assertEquals("()void",alloc.getMethod().getDescriptor());
        assertEquals("java.io.BufferedInputStream",alloc.getType());
        assertEquals("/new java.io.BufferedInputStream/0",alloc.getAllocSite());
    }

    @Test
    public void testObject2() throws Exception {
        String doopOutput =
                "<<immutable-hcontext>>\t<java.lang.ThreadLocal$ThreadLocalMap: void <init>(java.lang.ThreadLocal,java.lang.Object)>/new java.lang.ThreadLocal$ThreadLocalMap$Entry/0\t<<immutable-context>>\t<java.lang.ref.Reference: void <init>(java.lang.Object,java.lang.ref.ReferenceQueue)>/@this";
        Reader in = new StringReader(doopOutput);
        Pointsto pointsto = new PointstoBuilder().build(in,pointstoConfig);

        Set<Variable> variables = pointsto.getVariables();
        assertEquals(1,variables.size());

        // <java.lang.ref.Reference: void <init>(java.lang.Object,java.lang.ref.ReferenceQueue)>/@this
        Variable var = variables.iterator().next();
        assertEquals("java.lang.ref.Reference",var.getMethod().getClassName());
        assertEquals("<init>",var.getMethod().getMethodName());
        assertEquals("(java.lang.Object,java.lang.ref.ReferenceQueue)void",var.getMethod().getDescriptor());
        assertEquals("@this",var.getName());

        Collection<HObject> objects = pointsto.getObjects(var);
        assertEquals(1,objects.size());

        // <java.lang.ThreadLocal$ThreadLocalMap: void <init>(java.lang.ThreadLocal,java.lang.Object)>/new java.lang.ThreadLocal$ThreadLocalMap$Entry/0
        HObject obj = objects.iterator().next();
        assertTrue(obj instanceof Allocation);
        Allocation alloc = (Allocation)obj;
        assertEquals("java.lang.ThreadLocal$ThreadLocalMap",alloc.getMethod().getClassName());
        assertEquals("<init>",alloc.getMethod().getMethodName());
        assertEquals("(java.lang.ThreadLocal,java.lang.Object)void",alloc.getMethod().getDescriptor());
        assertEquals("java.lang.ThreadLocal$ThreadLocalMap$Entry",alloc.getType());
        assertEquals("/new java.lang.ThreadLocal$ThreadLocalMap$Entry/0",alloc.getAllocSite());
    }


    @Test
    public void testNull() throws Exception {
        String doopOutput =
                "<<immutable-hcontext>>\t<<null pseudo heap>>\t<<immutable-context>>\t<sun.util.resources.LocaleData: java.util.ResourceBundle getBreakIteratorInfo(java.util.Locale)>/@parameter0";
        Reader in = new StringReader(doopOutput);
        Pointsto pointsto = new PointstoBuilder().build(in,pointstoConfig);

        Set<Variable> variables = pointsto.getVariables();
        assertEquals(1,variables.size());

        // <sun.util.resources.LocaleData: java.util.ResourceBundle getBreakIteratorInfo(java.util.Locale)>/@parameter0
        Variable var = variables.iterator().next();
        assertEquals("sun.util.resources.LocaleData",var.getMethod().getClassName());
        assertEquals("getBreakIteratorInfo",var.getMethod().getMethodName());
        assertEquals("(java.util.Locale)java.util.ResourceBundle",var.getMethod().getDescriptor());
        assertEquals("@parameter0",var.getName());

        Collection<HObject> objects = pointsto.getObjects(var);
        assertEquals(1,objects.size());

        // <<null pseudo heap>>
        HObject obj = objects.iterator().next();
        assertTrue(obj instanceof Null);
        assertSame(Null.DEFAULT,obj);
    }

    //
    @Test
    public void testClassObject() throws Exception {
        String doopOutput =
                "<<immutable-hcontext>>\t<class java.lang.ClassLoader>\t<<immutable-context>>\t<java.lang.ref.WeakReference: void <init>(java.lang.Object,java.lang.ref.ReferenceQueue)>/@parameter0";
        Reader in = new StringReader(doopOutput);
        Pointsto pointsto = new PointstoBuilder().build(in,pointstoConfig);

        Set<Variable> variables = pointsto.getVariables();
        assertEquals(1,variables.size());

        // <java.lang.ref.WeakReference: void <init>(java.lang.Object,java.lang.ref.ReferenceQueue)>/@parameter0
        Variable var = variables.iterator().next();
        assertEquals("java.lang.ref.WeakReference",var.getMethod().getClassName());
        assertEquals("<init>",var.getMethod().getMethodName());
        assertEquals("(java.lang.Object,java.lang.ref.ReferenceQueue)void",var.getMethod().getDescriptor());
        assertEquals("@parameter0",var.getName());

        // <class java.lang.ClassLoader>
        Collection<HObject> objects = pointsto.getObjects(var);
        assertEquals(1,objects.size());

        HObject obj = objects.iterator().next();
        assertTrue(obj instanceof ClassObject);
        ClassObject klass = (ClassObject)obj;

        assertEquals("java.lang.ClassLoader",klass.getValue());
    }

    @Test
    public void testStringLiteral1() throws Exception {
        String doopOutput =
                "<<immutable-hcontext>>\tICU data file error: Not an ICU data file\t<<immutable-context>>\t<java.lang.Class: java.io.InputStream getResourceAsStream(java.lang.String)>/l1#2#_2217";
        Reader in = new StringReader(doopOutput);
        Pointsto pointsto = new PointstoBuilder().build(in,pointstoConfig);

        Set<Variable> variables = pointsto.getVariables();
        assertEquals(1,variables.size());

        // <java.lang.Class: java.io.InputStream getResourceAsStream(java.lang.String)>/l1#2#_2217
        Variable var = variables.iterator().next();
        assertEquals("java.lang.Class",var.getMethod().getClassName());
        assertEquals("getResourceAsStream",var.getMethod().getMethodName());
        assertEquals("(java.lang.String)java.io.InputStream",var.getMethod().getDescriptor());
        assertEquals("l1#2#_2217",var.getName());

        Collection<HObject> objects = pointsto.getObjects(var);
        assertEquals(1,objects.size());

        HObject obj = objects.iterator().next();
        assertTrue(obj instanceof StringLiteral);
        StringLiteral string = (StringLiteral)obj;

        assertEquals("ICU data file error: Not an ICU data file",string.getValue());
    }

    @Test
    public void testStringLiteral2() throws Exception {
        String doopOutput =
                "// <<immutable-hcontext>>\t<<\\\\\" Signature not available\\\\\">>\t<<immutable-context>>\t<java.security.Signature: java.security.Signature getInstance(java.lang.String)>/$stringconstant2";
        Reader in = new StringReader(doopOutput);
        Pointsto pointsto = new PointstoBuilder().build(in,pointstoConfig);

        Set<Variable> variables = pointsto.getVariables();
        assertEquals(1,variables.size());

        // <java.security.Signature: java.security.Signature getInstance(java.lang.String)>/$stringconstant2
        Variable var = variables.iterator().next();
        assertEquals("java.security.Signature",var.getMethod().getClassName());
        assertEquals("getInstance",var.getMethod().getMethodName());
        assertEquals("(java.lang.String)java.security.Signature",var.getMethod().getDescriptor());
        assertEquals("$stringconstant2",var.getName());

        Collection<HObject> objects = pointsto.getObjects(var);
        assertEquals(1,objects.size());

        HObject obj = objects.iterator().next();
        assertTrue(obj instanceof StringLiteral);
        StringLiteral string = (StringLiteral)obj;

        assertEquals("\\\\\" Signature not available\\\\",string.getValue());


    }

    @Test
    public void testAlias() throws Exception  {
        String doopOutput =
            "<<immutable-hcontext>>\t<java.lang.System: void initializeSystemClass()>/new java.io.BufferedInputStream/0\t<<immutable-context>>\t<java.io.BufferedInputStream: void <init>(java.io.InputStream)>/@this\n"+
            "<<immutable-hcontext>>\t<java.lang.System: void initializeSystemClass()>/new java.io.BufferedInputStream/0\t<<immutable-context>>\t<java.lang.System: void initializeSystemClass()>/$r12\n" +
            "<<immutable-hcontext>>\t<java.lang.System: void initializeSystemClass()>/new java.io.BufferedInputStream/0\t<<immutable-context>>\t<java.lang.System: void setIn0(java.io.InputStream)>/@parameter0\n" +
            "<<immutable-hcontext>>\t<java.lang.System: java.io.PrintStream newPrintStream(java.io.FileOutputStream,java.lang.String)>/new java.io.PrintStream/0\t<<immutable-context>>\t<java.lang.System: void setOut0(java.io.PrintStream)>/@parameter0\n";

        Reader in = new StringReader(doopOutput);
        Pointsto pointsto = new PointstoBuilder().build(in,pointstoConfig);

        Set<Variable> variables = pointsto.getVariables();
        assertEquals(4,variables.size());

        Variable var1 = pointsto.getVariables().stream().filter(var -> var.getName().equals("@this")).findAny().get();
        assertNotNull(var1);
        Variable var2 = pointsto.getVariables().stream().filter(var -> var.getName().equals("$r12")).findAny().get();
        assertNotNull(var2);
        Variable var3 = pointsto.getVariables().stream().filter(var -> var.getName().equals("@parameter0") && var.getMethod().getMethodName().equals("setIn0")).findAny().get();
        assertNotNull(var3);
        Variable var4 = pointsto.getVariables().stream().filter(var -> var.getName().equals("@parameter0") && var.getMethod().getMethodName().equals("setOut0")).findAny().get();
        assertNotNull(var4);

        assertTrue(pointsto.mayAlias(var1,var2));
        assertTrue(pointsto.mayAlias(var2,var3));
        assertTrue(pointsto.mayAlias(var1,var3));
        assertFalse(pointsto.mayAlias(var1,var4));
        assertFalse(pointsto.mayAlias(var2,var4));
        assertFalse(pointsto.mayAlias(var3,var4));
    }


}
