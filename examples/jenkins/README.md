##Jenkins Authentication

When fuzzing jenskins, check the log sequence generated when the server starts for warnings.

If running into authentication issues where a 302 is received, but a failed login is returned (as a `Location` header), ensure the password in `jenkins.properties` is correct.
By default, Jenkins uses the password found in the following file:
`.jenkins/secrets/initialAdminPassword`

See also: [Issue#15](https://bitbucket.org/jensdietrich/webfuzzer/issues/15/jenkins-authentication-issue)
