## Obtaining and Building Instrumented Jar

WebGoat is spring-boot-based. It does not use the default setup for jars, the customised setup is described in detail 
here: [https://bitbucket.org/jensdietrich/webgoat8.1-fuzz].

## Fuzzer Executable

The executable is `nz.ac.vuw.httpfuzz.jee.FuzzWebGoat81`.

##  Login (Manual)

1. load [http://localhost:8080/WebGoat/login]
2. register user if necessary (for fuzzing session, `username=scottscott, password=tigertiger` is used)

## Registration

POST [http://localhost:8080/WebGoat/register.mvc]
Content-Type: application/x-www-form-urlencoded

form data:

-  username: scottscott
-  password: scottscott
-  matchingPassword: tigertiger
-  agree: agree

on success: returns 302 with location  header [http://localhost:8080/WebGoat/attack] 
on failure (e.g. passwords dont match): returns 200 with content type `text/html`

## XSS  Lesson

1. [http://localhost:8080/WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/6]
2. locations to look for XSS:
   1. Search fields that echo a search string back to the user
   2. Input fields that echo user data
   3. Error messages that return user supplied text
   4. Hidden fields that contain user supplied data
   5. Any page that displays user supplied data
   6. Message boards
   7. Free form comments
   8. HTTP Headers
   
   
3. Vulnerable form / field  to attack:   
   ```html
   <form class="attack-form" accept-charset="UNKNOWN" method="GET" name="xss-5a" action="/WebGoat/CrossSiteScripting/attack5a">
    ..
        <tr>
            <td>Enter your credit card number:</td>
            <td><input name="field1" value="4128 3214 0002 1999" type="TEXT"></td>
        </tr>
    ```

all form values must be submitted, otherwise a NPE is raised:

```java
There was an unexpected error (type=Internal Server Error, status=500).
Request processing failed; nested exception is java.lang.NullPointerException
java.lang.NullPointerException
	at org.owasp.webgoat.xss.CrossSiteScriptingLesson5a.completed(CrossSiteScriptingLesson5a.java:52)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.base/java.lang.reflect.Method.invoke(Method.java:566)
	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:190)
	at 
```

URL to submit (from chrome developer tools): [http://localhost:8080/WebGoat/CrossSiteScripting/attack5a?QTY1=1&QTY2=1&QTY3=1&QTY4=1&field1=%3Cscript%3Econsole.log(%27foo%27)%3C%2Fscript%3E&field2=111] (form encoded)

`<script>` tag seems to get sanitised on the client. This returns:

```json
{
  "lessonCompleted" : true,
  "feedback" : "Well done, but console logs are not very impressive are they? Please continue.",
  "output" : "Thank you for shopping at WebGoat. <br \\/>You're support is appreciated<hr \\/><p>We have charged credit card:<script>console.log('foo')<\\/script><br \\/>                             ------------------- <br \\/>                               $1997.96",
  "assignment" : "CrossSiteScriptingLesson5a",
  "attemptWasMade" : true
}
```

The attack is triggered in the context of `WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/6`. 
This page has a form has the id `attack-form`. The json-encoded response from form submission is processed in `LessonContentview.js::onSuccessResponse` with a call to `renderOutput`. This 
injects it into the DOM with `this.$curOutput.html(polyglot.t(s) || "");`. This triggers the execution of the script. 


## Export Format for Affogado Integration 

Affogado: [https://doi.org/10.1145/3236454.3236502] .

```json
{
   "owner" : {
      "url" : "http://localhost:8080/WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/1",
      "id" : "form-1",
      "headers": [
          {"name":"header1","value": "header-value1"}, 
          {"name":"header2","value": "header-value2"} 
      ]
   }, 
   "method" : "GET",
   "values" : [
      {"name":"QTY1","value": "1"}, 
      {"name":"QTY2","value": "2"} 
   ],
  "headers": [
      {"name":"header3","value": "header-value3"}, 
      {"name":"header4","value": "header-value4"} 
  ]
}
```

