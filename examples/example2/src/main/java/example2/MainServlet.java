package example2;

import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.functors.InvokerTransformer;

import java.io.*;
import javax.servlet.http.*;

public class MainServlet extends HttpServlet {

    // field for testing
    public static boolean FOO_INVOKED = false;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String value = request.getParameter("method_name");
        Transformer transformer = new InvokerTransformer(value, new Class[]{}, new Object[]{});
        try {
            transformer.transform(new Foo());
        }
        catch (Exception x) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    public static class Foo {
        public void foo() {
            FOO_INVOKED = true;
        }
    }

}