This is an experimental web application fuzzing framework, consisting of a generic client, and advanced support for JEE-based servers, including static pre-analysis, and runtime feedback from the targeted web application.

Prerequisite: build this project, and the two subprojects `/war-preparer` and `/webfuzz-rt` with `mvn package`. Note: separate builds are required for those subprojects.

`ant` is used to run the preanalysis - fuzzing pipeline. To execute this, create an analysis parameter file (e.g., by cloning and editing `example1.properties`), and the run 
`ant -buildfile run.xml -propertyfile <name-of-propertyfile>`.  

Log files will by default be generated in `logs/` (log settings are in `log4j.properties`), and selected logs will be displayed on the console. 
It is highly recommended to check logs for warnings, in particular in the start-up sequence. If this (ANT-based) approach does not work, a custom Java class
can be implemented. The repository contains as base class to facilitate this (`nz.ac.vuw.httpfuzz.jee.AbstractFuzzer`), and 
an example used to fuzz `WebGoat-8.1` (`nz.ac.vuw.httpfuzz.jee.FuzzWebGoat81`).

### Static Pre-Analysis 

The application performs several static analyses in order to improve fuzzing throughput. 

Analyses the war file in order to extract entry point URLs. At the moment, only `web.xml` is analysed. Support for similar sources (web annotations, JSPs) will be added later.

At the moment, the following information is used: 

1. string literals 
2. constants used to query headers and request parameters

This information is used to build the *static model* that can be used to optimise requests, for instance, to compose GET requests using certain paths and query parameters. 


### Dynamic Analysis

For runtime analysis, the server application (war file) has to be instrumented. The instrumenter uses an agent (loadtime instrumentation),
the code is in the `webfuzz-rt` module that has its own readme. War files need to be  prepared for fuzzing, a script for this can be found in the `war-preparer`
module that again has its own readme with details. Information gathered at runtime is returned to the client va a special header (ticket) that can be used to fetch details
in a separate request. This includes information about methods called, and keys used when request parameters are accessed.

The following information is gathered using dynamic analysis: 

1. invoked methods
2. request parameter values used (at call site of `javax.servlet.http.HttpServletRequest::getParameter`)
3. stack traces of invocations of critical methods 

Information from 1. and 2. is used to build the *dynamic model*, which is used together with the *static model* to drive input generation.

### Input Generation

Input generation takes place in classes in `nz.ac.vuw.httpfuzz.http`. Generation is based on a containment hierachy of types:

```bash
RequestSpec
	|_  URISpec
	|_  MethodSpec
	|_  HeadersSpec
	    |_  HeaderSpec
   |_  ParametersSpec
       |_  ParameterSpec
       
```

The actual http request will then be generated from the `RequestSpec` instance. To create this instance (these instances), for each type in this hierachy a matching `*Generator` is used that extends `BiFunction<SourceOfRandomness,Context,T>`. Given a source of randomness and a context (which contains the dynamic and the static model), an object of type `T` is generarated. Those generators form a parallel hierachy.


This simplified `RequestSpecForm` illustrates the principle:

```java
    public RequestSpec apply(SourceOfRandomness sourceOfRandomness, Context context) {
        int highScoringRequestCount = context.getDynamicModel().getTopRequestSpecs().size();

        int[] split = null; // probabilities in percent -- {GENERATE,MUTATE,CROSSBREED,FORM}

        Collection<Form> forms = context.getDynamicModel().getForms();

        if (highScoringRequestCount==0) {
            split = forms.isEmpty()?new int[]{100,0,0,0}:new int[]{90,0,0,10};
        }
        else if (highScoringRequestCount<5) {
            split = forms.isEmpty()?new int[]{80,20,0,0}:new int[]{75,15,0,10};
        }
        else  {
            split = forms.isEmpty()?new int[]{60,20,20}:new int[]{55,18,17,10};
        }

        assert split.length==3;
        assert split[0] + split[1] + split[2] == 100;

        try {
            Supplier<RequestSpec> requestSpecs = ProbabilisticSupplierFactory.create()
                .withProbability(split[0]).createValue(() -> randomGenerator.apply(sourceOfRandomness, context))
                .withProbability(split[1]).createValue(() -> mutatingGenerator.apply(sourceOfRandomness, context))
                .withProbability(split[2]).createValue(() -> crossoverGenerator.apply(sourceOfRandomness, context))
                .withProbability(split[3]).createValue(() -> sourceOfRandomness.choose(forms).apply(sourceOfRandomness,context))
                .build();

            RequestSpec requestSpec = requestSpecs.get();
```

The `ProbabilisticSupplierFactory` is an untility to set the probabilties used to select a particular strategy. These probabilities can be adjusted. Mist of the work is delegated to specialised generators, or generators for parts of the request. 

This example also shows the interaction with the dynamic model: the dynamic model records, amogts other properties, forms returned by responses. These forms can then be used to generate new requests (using the request parameters encoded in forms). 




### License

The project is licensed under the The Universal Permissive License (UPL). This project is supported by Oracle Labs, Australia.

Copyright (c) 2019 Jens Dietrich





